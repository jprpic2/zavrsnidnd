package com.example.dndbeginner.Database.Room.Data.Ammunition;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ammunition")
public class AmmunitionEntry {
    @NonNull
    @PrimaryKey
    private String name;
    private int amount;
    private int cost;
    private int weight;

    public AmmunitionEntry(@NonNull String name, int amount, int cost, int weight) {
        this.name = name;
        this.amount = amount;
        this.cost = cost;
        this.weight = weight;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
