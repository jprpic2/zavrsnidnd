package com.example.dndbeginner.Database.Room.Data.Class;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.dndbeginner.Database.Room.Data.Background.BackgroundEntry;
import com.example.dndbeginner.Database.Room.Data.BaseDao;
import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;

import java.util.List;

@Dao
public interface ClassDao extends BaseDao<ClassEntry> {

    @Query("SELECT name FROM classes")
    List<String> getAllClassNames();

    @Query("SELECT classes.* FROM classes INNER JOIN playerCharacters ON classes.name=playerCharacters.class WHERE playerCharacters.id LIKE :id")
    ClassEntry getPlayerClass(int id);

    @Query("SELECT * FROM classes")
    List<ClassEntry> getClasses();

}