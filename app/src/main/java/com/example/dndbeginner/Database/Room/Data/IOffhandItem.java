package com.example.dndbeginner.Database.Room.Data;

public interface IOffhandItem {
    String getItemName();
    String getItemProperty();
}
