package com.example.dndbeginner.Database.Room.Data.Tools;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.dndbeginner.Database.Room.Data.IBackpackItem;

@Entity(tableName = "tools")
public class ToolEntry implements IBackpackItem {
    @NonNull
    @PrimaryKey
    private String name;
    @NonNull
    private String type;
    private int cost;
    private int weight;

    public ToolEntry(@NonNull String name, @NonNull String type, int cost, int weight) {
        this.name = name;
        this.type = type;
        this.cost = cost;
        this.weight = weight;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String getItemName() {
        return name;
    }

    @Override
    public String getItemDescription() {
        return type;
    }
}
