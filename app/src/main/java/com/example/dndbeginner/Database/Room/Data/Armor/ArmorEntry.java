package com.example.dndbeginner.Database.Room.Data.Armor;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "armor")
public class ArmorEntry {

    @PrimaryKey
    @NonNull
    private String name;
    @NonNull
    private String type;
    private int cost;
    @ColumnInfo(name = "armor_class")
    private int armorClass;
    private int strengthRequired;
    private boolean stealthDisadvantage;
    private int weight;

    public ArmorEntry(@NonNull String name, @NonNull String type, int cost, int armorClass, int strengthRequired, boolean stealthDisadvantage, int weight) {
        this.name = name;
        this.type = type;
        this.cost = cost;
        this.armorClass = armorClass;
        this.strengthRequired = strengthRequired;
        this.stealthDisadvantage = stealthDisadvantage;
        this.weight = weight;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(int armorClass) {
        this.armorClass = armorClass;
    }

    public int getStrengthRequired() {
        return strengthRequired;
    }

    public void setStrengthRequired(int strengthRequired) {
        this.strengthRequired = strengthRequired;
    }

    public boolean isStealthDisadvantage() {
        return stealthDisadvantage;
    }

    public void setStealthDisadvantage(boolean stealthDisadvantage) {
        this.stealthDisadvantage = stealthDisadvantage;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
