package com.example.dndbeginner.Database.Room.Data.Weapon;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.dndbeginner.Database.Room.Data.IOffhandItem;

import java.util.List;

@Entity(tableName = "weapons")
public class WeaponEntry implements IOffhandItem {
    @NonNull
    @PrimaryKey
    private String name;
    @NonNull
    private String type;
    @NonNull
    private String range;
    private int cost;
    @Nullable
    private String damage;
    @Nullable
    private String damageType;
    private int weight;
    @Nullable
    private List<String> properties;

    public WeaponEntry(@NonNull String name, @NonNull String type, @NonNull String range, int cost, @Nullable String damage, @Nullable String damageType, int weight, @Nullable List<String> properties) {
        this.name = name;
        this.type = type;
        this.range = range;
        this.cost = cost;
        this.damage = damage;
        this.damageType = damageType;
        this.weight = weight;
        this.properties = properties;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    @NonNull
    public String getRange() {
        return range;
    }

    public void setRange(@NonNull String range) {
        this.range = range;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Nullable
    public String getDamage() {
        return damage;
    }

    public void setDamage(@Nullable String damage) {
        this.damage = damage;
    }

    @Nullable
    public String getDamageType() {
        return damageType;
    }

    public void setDamageType(@Nullable String damageType) {
        this.damageType = damageType;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Nullable
    public List<String> getProperties() {
        return properties;
    }

    public void setProperties(@Nullable List<String> properties) {
        this.properties = properties;
    }

    @Override
    public String getItemName() {
        return this.name;
    }

    @Override
    public String getItemProperty() {
        if(this.properties.toString().toLowerCase().contains("light")){
            return "Dual wield light weapons";
        }
        else {
            return "Two-hand your main weapon";
        }
    }
}
