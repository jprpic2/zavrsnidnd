package com.example.dndbeginner.Database.Room.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.Room.Data.Ammunition.AmmunitionEntry;

import java.util.List;

public class AmmunitionViewModel extends AndroidViewModel {

    private static final String TAG = AmmunitionViewModel.class.getSimpleName();

    private LiveData<List<AmmunitionEntry>> ammunition;
    private AppDatabase database;

    public AmmunitionViewModel(@NonNull Application application) {
        super(application);
        database = AppDatabase.getInstance(this.getApplication());
        ammunition = database.ammunitionDao().loadAllAmmunition();
    }

    public LiveData<List<AmmunitionEntry>> getAmmunition() {
        return ammunition;
    }

    public LiveData<List<String>> getAmmunitionNames(){
        return database.ammunitionDao().loadAmmunitionNames();
    }
}