package com.example.dndbeginner.Database.Room.Data.Race;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.dndbeginner.Database.Room.Data.BaseDao;

import java.util.List;

@Dao
public interface RaceDao extends BaseDao<RaceEntry> {

    @Query("SELECT race_name FROM race WHERE baseRace LIKE :search ")
    List<String> getSubRaces(String search);

    @Query("SELECT race_name FROM race")
    List<String> getAllRaceNames();

    @Query("SELECT * FROM race WHERE race_name LIKE :search")
    RaceEntry findRace(String search);

    @Query("SELECT race.* FROM race "+
            "INNER JOIN playerCharacters ON race.race_name=playerCharacters.race WHERE playerCharacters.id LIKE :id")
    LiveData<RaceEntry> getPlayerRace(int id);

    @Query("SELECT DISTINCT baseRace FROM race")
    List<String> getDistinctNames();
}