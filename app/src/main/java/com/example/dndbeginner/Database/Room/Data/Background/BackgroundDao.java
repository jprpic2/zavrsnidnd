package com.example.dndbeginner.Database.Room.Data.Background;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.dndbeginner.Database.Room.Data.Armor.ArmorEntry;
import com.example.dndbeginner.Database.Room.Data.BaseDao;

import java.util.List;

@Dao
public interface BackgroundDao extends BaseDao<BackgroundEntry> {

    @Query("SELECT * FROM background")
    List<BackgroundEntry> loadAllBackgrounds();

    @Query("SELECT name FROM background")
    List<String> loadAllBackgroundNames();

}