package com.example.dndbeginner.Database.Room.Data.OwnedItem;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;

@Entity(tableName = "ownedItem",foreignKeys = {@ForeignKey(entity = PlayerCharacterEntry.class,
        parentColumns = "id",
        childColumns = "ownerID",
        onDelete = ForeignKey.CASCADE)}, indices = {@Index(value = {"ownerID"})})
public class OwnedItemEntry {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String name;
    @NonNull
    private String type;
    private int ownerID;
    private int equipped;
    private int offhand;

    @Ignore
    public OwnedItemEntry(@NonNull String name, @NonNull String type, int ownerID) {
        this.name = name;
        this.type = type;
        this.ownerID = ownerID;
        this.equipped = 0;
        this.offhand = 0;
    }

    @Ignore
    public OwnedItemEntry(@NonNull String name, @NonNull String type, int ownerID, int equipped, int offhand) {
        this.name = name;
        this.type = type;
        this.ownerID = ownerID;
        this.equipped = equipped;
        this.offhand = offhand;
    }

    public OwnedItemEntry(int id, @NonNull String name, @NonNull String type, int ownerID, int equipped, int offhand) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.ownerID = ownerID;
        this.equipped=equipped;
        this.offhand = offhand;
    }


    public boolean isOffhand() {
        return offhand > 0;
    }

    public void setOffhand() {
        this.offhand = 1;
    }

    public int getOffhand(){
        return this.offhand;
    }

    public void unEquipOffhand() {
        this.offhand=0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }


    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public int getEquipped(){
        return equipped;
    }

    public boolean isEquipped() {
        return equipped > 0;
    }

    public void setEquipped() {
        this.equipped = 1;
    }

    public void unEquip() {
        this.equipped=0;
    }


}