package com.example.dndbeginner.Database.Room.Data.OwnedItem;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;

import androidx.room.Query;
import com.example.dndbeginner.Database.Room.Data.BaseDao;
import com.example.dndbeginner.Database.Room.Data.CustomItem.CustomItemEntry;

import java.util.List;

@Dao
public interface OwnedItemDao extends BaseDao<OwnedItemEntry> {

    @Query("SELECT ownedItem.* FROM ownedItem INNER JOIN playerCharacters ON ownedItem.ownerID=playerCharacters.id WHERE ownerID LIKE :id AND ownedItem.name NOT LIKE 'shield' AND type = 'armor' ORDER BY name ASC")
    LiveData<List<OwnedItemEntry>> getOwnedArmor(int id);

    @Query("SELECT ownedItem.* FROM ownedItem INNER JOIN playerCharacters ON ownedItem.ownerID=playerCharacters.id WHERE ownerID LIKE :id AND type LIKE 'tool'")
    LiveData<List<OwnedItemEntry>> getOwnedTools(int id);

    @Query("SELECT ownedItem.* FROM ownedItem INNER JOIN playerCharacters ON ownedItem.ownerID=playerCharacters.id WHERE ownerID LIKE :id AND type = 'weapon' ORDER BY name ASC")
    LiveData<List<OwnedItemEntry>> getOwnedWeapons(int id);

    @Query("SELECT ownedItem.* FROM ownedItem INNER JOIN playerCharacters ON ownedItem.ownerID=playerCharacters.id WHERE ownerID LIKE :id AND type LIKE 'shield'")
    LiveData<List<OwnedItemEntry>> getOwnedShields(int id);

    @Query("SELECT custom_items.* FROM custom_items WHERE custom_items.ownerID = :id")
    LiveData<List<CustomItemEntry>> getBackpackCustomItems(int id);

    @Query("SELECT ownedItem.* FROM ownedItem INNER JOIN playerCharacters ON ownedItem.ownerID=playerCharacters.id WHERE ownerID LIKE :id AND (type LIKE 'shield' OR type LIKE 'weapon')")
    LiveData<List<OwnedItemEntry>> getOwnedWeaponsAndShields(int id);

    @Query("SELECT ownedItem.* FROM ownedItem INNER JOIN playerCharacters ON ownedItem.ownerID=playerCharacters.id WHERE ownerID LIKE :id AND ownedItem.offhand = 1")
    OwnedItemEntry getEquippedOffhand(int id);
}

