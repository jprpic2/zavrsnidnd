package com.example.dndbeginner.Database.Room.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.Room.Data.Armor.ArmorEntry;

import java.util.List;

public class ArmorViewModel extends AndroidViewModel {

    private static final String TAG = ArmorViewModel.class.getSimpleName();

    private LiveData<List<ArmorEntry>> armor;

    AppDatabase database;


    public ArmorViewModel(@NonNull Application application) {
        super(application);
        database = AppDatabase.getInstance(this.getApplication());
        armor = database.armorDao().loadAllArmor();
    }

    public LiveData<List<ArmorEntry>> getArmor() {
        return armor;
    }

    public LiveData<List<String>> getArmorNames(){
        return database.armorDao().loadArmorNames();
    }

    public LiveData<List<ArmorEntry>> getOwnedArmor(List<String> names){
        return database.armorDao().getOwnedArmor(names);
    }
}
