package com.example.dndbeginner.Database.Room.Data.Class;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Arrays;
import java.util.List;

@Entity(tableName = "classes")
public class ClassEntry {

    @NonNull
    @PrimaryKey
    private String name;
    private int hitDice;
    @Nullable
    private List<String> armorProficiency;
    @NonNull
    private List<String> weaponProficiency;
    @Nullable
    private List<String> toolProficiency;
    @NonNull
    private List<String> savingThrows;
    private int numberOfSkills;
    @NonNull
    private List<String> availableSkills;
    @NonNull
    String primaryAbilities;
    int numberOfTools;
    int numberOfMusic;
    List<String> startingEquipment;

    public ClassEntry(@NonNull String name, int hitDice, @Nullable List<String> armorProficiency, @NonNull List<String> weaponProficiency, @Nullable List<String> toolProficiency, @NonNull List<String> savingThrows, int numberOfSkills, @NonNull List<String> availableSkills, @NonNull String primaryAbilities, int numberOfTools, int numberOfMusic, List<String> startingEquipment) {
        this.name = name;
        this.hitDice = hitDice;
        this.armorProficiency = armorProficiency;
        this.weaponProficiency = weaponProficiency;
        this.toolProficiency = toolProficiency;
        this.savingThrows = savingThrows;
        this.numberOfSkills = numberOfSkills;
        this.availableSkills = availableSkills;
        this.primaryAbilities = primaryAbilities;
        this.numberOfTools = numberOfTools;
        this.numberOfMusic = numberOfMusic;
        this.startingEquipment = startingEquipment;
    }

    @Ignore
    public ClassEntry() {
        this.name = "Barbarian";
        this.hitDice = 12;
        this.armorProficiency = Arrays.asList("Light armor", "medium armor", "shields");
        this.weaponProficiency = Arrays.asList("Simple weapons", "martial weapons");
        this.toolProficiency = null;
        this.savingThrows = Arrays.asList("STR","CON");
        this.numberOfSkills = 2;
        this.availableSkills = Arrays.asList("Animal Handling", "Athletics", "Intimidation", "Nature", "Perception", "Survival");
        this.primaryAbilities = "Strength";
        this.numberOfTools = 0;
        this.numberOfMusic = 0;
        this.startingEquipment=Arrays.asList("No Armor", "Greataxe", "Handaxe", "Handaxe", "Explorer's Pack", "Javelin", "Javelin","Javelin","Javelin");
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getHitDice() {
        return hitDice;
    }

    public void setHitDice(int hitDice) {
        this.hitDice = hitDice;
    }

    @Nullable
    public List<String> getArmorProficiency() {
        return armorProficiency;
    }

    public void setArmorProficiency(@Nullable List<String> armorProficiency) {
        this.armorProficiency = armorProficiency;
    }

    @NonNull
    public List<String> getWeaponProficiency() {
        return weaponProficiency;
    }

    public void setWeaponProficiency(@NonNull List<String> weaponProficiency) {
        this.weaponProficiency = weaponProficiency;
    }

    @Nullable
    public List<String> getToolProficiency() {
        return toolProficiency;
    }

    public void setToolProficiency(@Nullable List<String> toolProficiency) {
        this.toolProficiency = toolProficiency;
    }

    @NonNull
    public List<String> getSavingThrows() {
        return savingThrows;
    }

    public void setSavingThrows(@NonNull List<String> savingThrows) {
        this.savingThrows = savingThrows;
    }

    public int getNumberOfSkills() {
        return numberOfSkills;
    }

    public void setNumberOfSkills(int numberOfSkills) {
        this.numberOfSkills = numberOfSkills;
    }

    @NonNull
    public List<String> getAvailableSkills() {
        return availableSkills;
    }

    public void setAvailableSkills(@NonNull List<String> availableSkills) {
        this.availableSkills = availableSkills;
    }

    @NonNull
    public String getPrimaryAbilities() {
        return primaryAbilities;
    }

    public void setPrimaryAbilities(@NonNull String primaryAbilities) {
        this.primaryAbilities = primaryAbilities;
    }

    public int getNumberOfTools() {
        return numberOfTools;
    }

    public void setNumberOfTools(int numberOfTools) {
        this.numberOfTools = numberOfTools;
    }

    public int getNumberOfMusic() {
        return numberOfMusic;
    }

    public void setNumberOfMusic(int numberOfMusic) {
        this.numberOfMusic = numberOfMusic;
    }

    public List<String> getStartingEquipment() {
        return startingEquipment;
    }

    public void setStartingEquipment(List<String> startingEquipment) {
        this.startingEquipment = startingEquipment;
    }
}
