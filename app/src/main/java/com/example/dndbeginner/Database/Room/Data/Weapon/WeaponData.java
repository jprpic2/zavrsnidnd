package com.example.dndbeginner.Database.Room.Data.Weapon;

import java.util.Arrays;
import java.util.Collections;

public class WeaponData {
    public static WeaponEntry[] populateWeaponData(){
        return new WeaponEntry[]{
                new WeaponEntry ("Club","Simple","Melee",20,"1d4","bludgeoning",2, Collections.singletonList("Light")),
        new WeaponEntry ("Dagger","Simple","Melee",200,"1d4","piercing",1,Arrays.asList("Finesse", "light", "thrown (range 20/60)")),
        new WeaponEntry ("Greatclub","Simple","Melee",20,"1d8","bludgeoning",10, Collections.singletonList("Two-handed")),
        new WeaponEntry ("Handaxe","Simple","Melee",500,"1d6","slashing",2,Arrays.asList("Light", "thrown (range 20/60)")),
        new WeaponEntry ("Javelin","Simple","Melee",50,"1d6","piercing",2, Collections.singletonList("Thrown (range 30/120)")),
        new WeaponEntry ("Light Hammer","Simple","Melee",200,"1d4","bludgeoning",2,Arrays.asList("Light", "thrown (range 20/60)")),
        new WeaponEntry ("Mace","Simple","Melee",500,"1d6","bludgeoning",4,null),
        new WeaponEntry ("Quarterstaff","Simple","Melee",20,"1d6","bludgeoning",4, Collections.singletonList("Versatile (1d8)")),
        new WeaponEntry ("Sickle","Simple","Melee",100,"1d4","slashing",2, Collections.singletonList("Light")),
        new WeaponEntry ("Spear","Simple","Melee",100,"1d6","piercing",3,Arrays.asList("Thrown (range 20/60)", "versatile (1d8)")),
        new WeaponEntry ("Crossbow, light","Simple","Ranged",2500,"1d8","piercing",5,Arrays.asList("Ammunition (range 80/320)", "loading", "two-handed")),
        new WeaponEntry ("Dart","Simple","Ranged",5,"1d4","piercing",0,Arrays.asList("Finesse", "thrown (range 20/60)")),
        new WeaponEntry ("Shortbow","Simple","Ranged",2500,"1d6","piercing",2, Collections.singletonList("Ammunition (range 80/320), two-handed")),
        new WeaponEntry ("Sling","Simple","Ranged",10,"1d4","bludgeoning",0, Collections.singletonList("Ammunition (range 30/120)")),
        new WeaponEntry ("Battleaxe","Martial","Melee",1000,"1d8","slashing",4, Collections.singletonList("Versatile (1d10)")),
        new WeaponEntry ("Flail","Martial","Melee",1000,"1d8","bludgeoning",2,null),
        new WeaponEntry ("Glaive","Martial","Melee",2000,"1d10","slashing",6,Arrays.asList("Heavy", "reach", "two-handed")),
        new WeaponEntry ("Greataxe","Martial","Melee",3000,"1d12","slashing",7,Arrays.asList("Heavy", "two-handed")),
        new WeaponEntry ("Greatsword","Martial","Melee",5000,"2d6","slashing",6,Arrays.asList("Heavy", "two-handed")),
        new WeaponEntry ("Halberd","Martial","Melee",2000,"1d10","slashing",6,Arrays.asList("Heavy", "reach", "two-handed")),
        new WeaponEntry ("Lance","Martial","Melee",1000,"1d12","piercing",6, Collections.singletonList("Special")),
        new WeaponEntry ("Longsword","Martial","Melee",1500,"1d8","slashing",3, Collections.singletonList("Versatile (1d10)")),
        new WeaponEntry ("Maul","Martial","Melee",1000,"2d6","bludgeoning",10,Arrays.asList("Heavy", "two-handed")),
        new WeaponEntry ("Morningstar","Martial","Melee",1500,"1d8","piercing",4,null),
        new WeaponEntry ("Pike","Martial","Melee",500,"1d10","piercing",18,Arrays.asList("Heavy", "reach", "two-handed")),
        new WeaponEntry ("Rapier","Martial","Melee",2500,"1d8","piercing",2, Collections.singletonList("Finesse")),
        new WeaponEntry ("Scimitar","Martial","Melee",2500,"1d6","slashing",3,Arrays.asList("Finesse", "light")),
        new WeaponEntry ("Shortsword","Martial","Melee",1000,"1d6","piercing",2,Arrays.asList("Finesse", "light")),
        new WeaponEntry ("Trident","Martial","Melee",500,"1d6","piercing",4,Arrays.asList("Thrown (range 20/60)", "versatile (1d8)")),
        new WeaponEntry ("War pick","Martial","Melee",500,"1d8","piercing",2,null),
        new WeaponEntry ("Warhammer","Martial","Melee",1500,"1d8","bludgeoning",2, Collections.singletonList("Versatile (1d10)")),
        new WeaponEntry ("Whip","Martial","Melee",200,"1d4","slashing",3,Arrays.asList("Finesse", "reach")),
        new WeaponEntry ("Blowgun","Martial","Ranged",1000,"1","piercing",1,Arrays.asList("Ammunition (range 25/100)", "loading")),
        new WeaponEntry ("Crossbow, hand","Martial","Ranged",7500,"1d6","piercing",3,Arrays.asList("Ammunition (range 30/120)", "light", "loading")),
        new WeaponEntry ("Crossbow, heavy","Martial","Ranged",5000,"1d10","piercing",18,Arrays.asList("Ammunition (range 100/400)", "heavy", "loading", "two-handed")),
        new WeaponEntry ("Longbow","Martial","Ranged",5000,"1d8","piercing",2,Arrays.asList("Ammunition (range 150/600)", "heavy", "two-handed")),
        new WeaponEntry ("Net","Martial","Ranged",100,null,null,3,Arrays.asList("Special", "thrown (range 5/15)")),
        };
    }
}
