package com.example.dndbeginner.Database.Room.Data.Ammunition;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.dndbeginner.Database.Room.Data.BaseDao;

import java.util.List;

@Dao
public interface AmmunitionDao extends BaseDao<AmmunitionEntry> {

    @Query("SELECT * FROM ammunition")
    LiveData<List<AmmunitionEntry>> loadAllAmmunition();

    @Query("SELECT name FROM ammunition")
    LiveData<List<String>> loadAmmunitionNames();
}