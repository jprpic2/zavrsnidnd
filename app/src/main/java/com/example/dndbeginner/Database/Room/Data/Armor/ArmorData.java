package com.example.dndbeginner.Database.Room.Data.Armor;

public class ArmorData {
    public static ArmorEntry[] populateArmorData(){
        return new ArmorEntry[]{
                new ArmorEntry("No Armor","Light",0,10,0,false,0),
                new ArmorEntry("Padded","Light",5,11,0,true,8),
                new ArmorEntry("Leather","Light",10,11,0,false,10),
                new ArmorEntry("Studded Leather","Light",45,12,0,false,13),
                new ArmorEntry("Hide","Medium",10,12,0,false,12),
                new ArmorEntry("Chain Shirt","Medium",50,13,0,false,20),
                new ArmorEntry("Scale Mail","Medium",50,14,0,true,45),
                new ArmorEntry("Breastplate","Medium",400,14,0,false,20),
                new ArmorEntry("Half Plate","Medium",750,15,0,true,40),
                new ArmorEntry("Ring Mail","Heavy",30,14,0,true,40),
                new ArmorEntry("Chain Mail","Heavy",75,16,13,true,55),
                new ArmorEntry("Splint","Heavy",200,17,15,true,60),
                new ArmorEntry("Plate","Heavy",1500,18,15,true,65),
                new ArmorEntry("Shield","Shield",10,2,0,false,6),
                new ArmorEntry("Wooden Shield","Shield",10,2,0,false,6),
        };
    }
}