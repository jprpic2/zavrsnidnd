package com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;

public class PlayerCharacterViewModel extends ViewModel {

    private LiveData<PlayerCharacterEntry> playerCharacterEntry;

    public PlayerCharacterViewModel(AppDatabase database, int id) {
        playerCharacterEntry = database.playerCharacterDao().getPlayerCharacter(id);
    }

    public LiveData<PlayerCharacterEntry> getPlayerCharacterEntry() {
        return playerCharacterEntry;
    }
}
