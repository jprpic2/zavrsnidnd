package com.example.dndbeginner.Database.Room.Data.Weapon;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import com.example.dndbeginner.Database.Room.Data.BaseDao;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolEntry;


import java.util.List;

@Dao
public interface WeaponDao extends BaseDao<WeaponEntry> {

    @Query("SELECT * FROM weapons WHERE properties LIKE '%' || 'Light' || '%'")
    LiveData<List<WeaponEntry>> getLightWeapons();

    @Query("SELECT name FROM weapons")
    LiveData<List<String>> loadWeaponNames();

    @Query("SELECT * from weapons WHERE name IN (:names)")
    LiveData<List<WeaponEntry>> getOwnedWeapons(List<String> names);

    @Query("SELECT * FROM weapons")
    LiveData<List<WeaponEntry>> getWeapons();

    @Query("SELECT name FROM weapons")
    List<String> getWeaponNames();

    @Query("SELECT * FROM weapons WHERE name IN (:weaponsNames) AND properties LIKE '%' || 'Light' || '%'  ")
    List<WeaponEntry> getOwnedLightWeapons(List<String> weaponsNames);

    @Query("SELECT * FROM weapons WHERE name = :name AND properties LIKE '%' || 'Versatile' || '%'")
    WeaponEntry getEquippedVersatileWeapon(String name);

    @Query("SELECT * from weapons WHERE name = :name")
    WeaponEntry getWeapon(String name);
}