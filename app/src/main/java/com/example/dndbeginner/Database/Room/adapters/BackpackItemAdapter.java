package com.example.dndbeginner.Database.Room.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dndbeginner.Database.Room.Data.IBackpackItem;
import com.example.dndbeginner.R;

import java.util.List;

public class BackpackItemAdapter extends RecyclerView.Adapter<BackpackItemAdapter.BackpackViewHolder> {

    List<IBackpackItem> backpackItems;

    public BackpackItemAdapter(List<IBackpackItem> backpackItems){
        this.backpackItems=backpackItems;
    }

    @NonNull
    @Override
    public BackpackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.backpack_item,parent,false);
        return new BackpackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BackpackViewHolder holder, int position) {
        IBackpackItem backpackItem = backpackItems.get(position);
        if(backpackItem!=null){
            holder.itemName.setText(backpackItem.getItemName());
            holder.itemDescription.setText(backpackItem.getItemDescription());
        }
    }

    public List<IBackpackItem> getBackpackItems() {
        return backpackItems;
    }

    public void setBackpackItems(List<IBackpackItem> backpackItems) {
        this.backpackItems = backpackItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return backpackItems.size();
    }

    public static class BackpackViewHolder extends RecyclerView.ViewHolder{

        TextView itemName;
        TextView itemDescription;

        public BackpackViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName=itemView.findViewById(R.id.backpack_item_name);
            itemDescription=itemView.findViewById(R.id.backpack_item_description);
        }
    }
}
