package com.example.dndbeginner.Database.Room.Data.Shield;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.dndbeginner.Database.Room.Data.IOffhandItem;

@Entity(tableName = "shield")
public class ShieldEntry implements IOffhandItem {
    @PrimaryKey
    @NonNull
    String name;
    int cost;
    int weight;

    public ShieldEntry(@NonNull String name, int cost, int weight) {
        this.name = name;
        this.cost = cost;
        this.weight = weight;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String getItemName() {
        return this.name;
    }

    @Override
    public String getItemProperty() {
        return "Increase armor class by 2";
    }
}
