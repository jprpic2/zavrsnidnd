package com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter;


import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.example.dndbeginner.Database.AppDatabase;
public class PlayerCharacterViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final AppDatabase mDb;
    private final int mId;

    public PlayerCharacterViewModelFactory(AppDatabase database, int id) {
        mDb = database;
        mId = id;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new PlayerCharacterViewModel(mDb, mId);
    }

}
