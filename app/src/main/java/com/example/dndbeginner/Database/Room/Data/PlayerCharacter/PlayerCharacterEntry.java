package com.example.dndbeginner.Database.Room.Data.PlayerCharacter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.example.dndbeginner.Database.Room.Data.Background.BackgroundEntry;
import com.example.dndbeginner.Database.Room.Data.Class.ClassEntry;
import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Entity(tableName = "playerCharacters",
        foreignKeys = {@ForeignKey(entity = RaceEntry.class,
            parentColumns = "race_name",
            childColumns = "race",
            onDelete = ForeignKey.CASCADE),
        @ForeignKey(entity = ClassEntry.class,
            parentColumns = "name",
            childColumns = "class",
            onDelete = ForeignKey.CASCADE),
        @ForeignKey(entity = BackgroundEntry.class,
                parentColumns = "name",
                childColumns = "background",
                onDelete = ForeignKey.CASCADE)},
        indices = {@Index(value = {"race"}),
                @Index(value = {"class"}),
                @Index(value = {"background"})})
public class PlayerCharacterEntry {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String name;
    @ColumnInfo(name = "character_level")
    private int characterLevel;
    private int experience;
    @NonNull
    private String race;
    @NonNull
    @ColumnInfo(name = "class")
    private String characterClass;
    @NonNull
    private String background;
    @NonNull
    private List<String> skills;
    private int strength;
    private int dexterity;
    private int constitution;
    private int intelligence;
    private int wisdom;
    private int charisma;
    @ColumnInfo(name = "armor_class")
    private int armorClass;
    private String attackDie;
    private int hitPoints;
    @Nullable
    private List<String> tools;
    @NonNull
    private List<String> languages;
    @Nullable
    private List<String> spells;
    private int gold;
    private int silver;
    private int copper;


    @Ignore
    public PlayerCharacterEntry() {
        this.name = "Unknown";
        this.characterLevel = 1;
        this.experience = 0;
        this.race = "Dragonborn";
        this.characterClass = "Barbarian";
        this.background = "Acolyte";
        this.skills = Arrays.asList("Insight","Religion");
        this.strength = 8;
        this.dexterity = 8;
        this.constitution = 8;
        this.intelligence = 8;
        this.wisdom = 8;
        this.charisma = 8;
        this.armorClass = 0;
        this.hitPoints = 12;
        this.tools = Collections.singletonList(null);
        this.languages = Collections.singletonList(null);
        this.spells = Collections.singletonList(null);
        this.gold = 0;
        this.silver = 0;
        this.copper = 0;

    }

    @Ignore
    public PlayerCharacterEntry(@NonNull String name, int characterLevel, int experience, @NonNull String race, @NonNull String characterClass, @NonNull String background, @NonNull List<String> skills, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma, int armorClass, String attackDie, int hitPoints, @Nullable List<String> tools, @NonNull List<String> languages, @Nullable List<String> spells, int gold, int silver, int copper) {
        this.name = name;
        this.characterLevel = characterLevel;
        this.experience = experience;
        this.race = race;
        this.characterClass = characterClass;
        this.background = background;
        this.skills = skills;
        this.strength = strength;
        this.dexterity = dexterity;
        this.constitution = constitution;
        this.intelligence = intelligence;
        this.wisdom = wisdom;
        this.charisma = charisma;
        this.armorClass = armorClass;
        this.attackDie=attackDie;
        this.hitPoints = hitPoints;
        this.tools = tools;
        this.languages = languages;
        this.spells=spells;
        this.gold = gold;
        this.silver = silver;
        this.copper = copper;
    }



    public PlayerCharacterEntry(int id, @NonNull String name, int characterLevel, int experience, @NonNull String race, @NonNull String characterClass, @NonNull String background, @NonNull List<String> skills, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma, int armorClass, String attackDie, int hitPoints, @Nullable List<String> tools, @NonNull List<String> languages, @Nullable List<String> spells, int gold, int silver, int copper) {
        this.id = id;
        this.name = name;
        this.characterLevel = characterLevel;
        this.experience = experience;
        this.race = race;
        this.characterClass = characterClass;
        this.background = background;
        this.skills = skills;
        this.strength = strength;
        this.dexterity = dexterity;
        this.constitution = constitution;
        this.intelligence = intelligence;
        this.wisdom = wisdom;
        this.charisma = charisma;
        this.armorClass = armorClass;
        this.attackDie=attackDie;
        this.hitPoints = hitPoints;
        this.tools = tools;
        this.languages = languages;
        this.spells = spells;
        this.gold = gold;
        this.silver = silver;
        this.copper = copper;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getCharacterLevel() {
        return characterLevel;
    }

    public void setCharacterLevel(int characterLevel) {
        this.characterLevel = characterLevel;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    @NonNull
    public String getRace() {
        return race;
    }

    public void setRace(@NonNull String race) {
        this.race = race;
    }

    @NonNull
    public String getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(@NonNull String characterClass) {
        this.characterClass = characterClass;
    }

    @NonNull
    public String getBackground() {
        return background;
    }

    public void setBackground(@NonNull String background) {
        this.background = background;
    }

    @NonNull
    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(@NonNull List<String> skills) {
        this.skills = skills;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(int armorClass) {
        this.armorClass = armorClass;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    @Nullable
    public List<String> getTools() {
        return tools;
    }

    public void setTools(@Nullable List<String> tools) {
        this.tools = tools;
    }

    @NonNull
    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(@NonNull List<String> languages) {
        this.languages = languages;
    }

    @Nullable
    public List<String> getSpells() {
        return spells;
    }

    public void setSpells(@Nullable List<String> spells) {
        this.spells = spells;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getSilver() {
        return silver;
    }

    public void setSilver(int silver) {
        this.silver = silver;
    }

    public int getCopper() {
        return copper;
    }

    public void setCopper(int copper) {
        this.copper = copper;
    }

    public String getAttackDie() {
        return attackDie;
    }

    public void setAttackDie(String attackDie) {
        this.attackDie = attackDie;
    }
}
