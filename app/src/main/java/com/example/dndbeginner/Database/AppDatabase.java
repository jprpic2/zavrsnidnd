package com.example.dndbeginner.Database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.dndbeginner.Database.Room.Data.Ammunition.AmmunitionDao;
import com.example.dndbeginner.Database.Room.Data.Ammunition.AmmunitionData;
import com.example.dndbeginner.Database.Room.Data.Ammunition.AmmunitionEntry;
import com.example.dndbeginner.Database.Room.Data.Armor.ArmorDao;
import com.example.dndbeginner.Database.Room.Data.Armor.ArmorData;
import com.example.dndbeginner.Database.Room.Data.Armor.ArmorEntry;
import com.example.dndbeginner.Database.Room.Data.Background.BackgroundDao;
import com.example.dndbeginner.Database.Room.Data.Background.BackgroundData;
import com.example.dndbeginner.Database.Room.Data.Background.BackgroundEntry;
import com.example.dndbeginner.Database.Room.Data.Class.ClassDao;
import com.example.dndbeginner.Database.Room.Data.Class.ClassData;
import com.example.dndbeginner.Database.Room.Data.Class.ClassEntry;
import com.example.dndbeginner.Database.Room.Data.CustomItem.CustomItemDao;
import com.example.dndbeginner.Database.Room.Data.CustomItem.CustomItemEntry;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemDao;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterDao;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.Data.Race.RaceDao;
import com.example.dndbeginner.Database.Room.Data.Race.RaceData;
import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;
import com.example.dndbeginner.Database.Room.Data.Shield.ShieldDao;
import com.example.dndbeginner.Database.Room.Data.Shield.ShieldData;
import com.example.dndbeginner.Database.Room.Data.Shield.ShieldEntry;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolDao;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolData;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolEntry;
import com.example.dndbeginner.Database.Room.Data.Weapon.WeaponDao;
import com.example.dndbeginner.Database.Room.Data.Weapon.WeaponData;
import com.example.dndbeginner.Database.Room.Data.Weapon.WeaponEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;


@Database(entities = {ArmorEntry.class, RaceEntry.class, ToolEntry.class, BackgroundEntry.class, ClassEntry.class, AmmunitionEntry.class, PlayerCharacterEntry.class, WeaponEntry.class, OwnedItemEntry.class, CustomItemEntry.class, ShieldEntry.class},version = 1,exportSchema = false)
@TypeConverters(StringListConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    private static final String LOG_TAG = AppDatabase.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "dnddata.db";
    private static AppDatabase sInstance;

    public static AppDatabase getInstance(final Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                Log.d(LOG_TAG, "Creating new database instance");
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase.class, AppDatabase.DATABASE_NAME)
                        .addCallback(new Callback() {
                            @Override
                            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                super.onCreate(db);
                                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        AppDatabase instance=getInstance(context);
                                        populateDatabase(instance);
                                        createEquippedWeaponTrigger(instance);
                                        //createEquippedArmorTrigger(instance);

                                    }
                                });
                            }
                        })
                        .build();
            }
        }
        return sInstance;
    }

    private static void createEquippedArmorTrigger(AppDatabase instance) {
        instance.getOpenHelper().getWritableDatabase().execSQL(
                "CREATE TRIGGER update_armor_class AFTER UPDATE ON ownedItem\n" +
                        "WHEN NEW.type = 'armor' AND NEW.equipped=1\n" +
                        "BEGIN\n" +
                        "UPDATE playerCharacters\n" +
                        "SET armor_class=(SELECT armor_class FROM armor INNER JOIN ownedItem ON armor.name=NEW.name) + (SELECT\n" +
                        "CASE\n" +
                        "\tWHEN armor.type='Light'\n" +
                        "    \tTHEN\n" +
                        "        \t(SELECT\n" +
                        "             CASE \n" +
                        "            \tWHEN playerCharacters.dexterity=1 THEN -5\n" +
                        "                WHEN playerCharacters.dexterity<4 THEN -4\n" +
                        "                WHEN playerCharacters.dexterity<6 THEN -3\n" +
                        "                when playerCharacters.dexterity<8 THEN -2\n" +
                        "                WHEN playerCharacters.dexterity<10 THEN -1\n" +
                        "                WHEN playerCharacters.dexterity<12 THEN 0\n" +
                        "                WHEN playerCharacters.dexterity<14 THEN 1\n" +
                        "                WHEN playerCharacters.dexterity<16 THEN 2\n" +
                        "                WHEN playerCharacters.dexterity<18 THEN 3\n" +
                        "                WHEN playerCharacters.dexterity<20 THEN 4\n" +
                        "                WHEN playerCharacters.dexterity=20 THEN 5\n" +
                        "             END\n" +
                        "             FROM playerCharacters WHERE playerCharacters.id=new.ownerID)\n" +
                        "    WHEN armor.type='Medium'\n" +
                        "    \tTHEN\n" +
                        "        \t(SELECT\n" +
                        "             CASE\n" +
                        "            \tWHEN playerCharacters.dexterity=1 THEN -5\n" +
                        "                WHEN playerCharacters.dexterity<4 THEN -4\n" +
                        "                WHEN playerCharacters.dexterity<6 THEN -3\n" +
                        "                when playerCharacters.dexterity<8 THEN -2\n" +
                        "                WHEN playerCharacters.dexterity<10 THEN -1\n" +
                        "                WHEN playerCharacters.dexterity<12 THEN 0\n" +
                        "                WHEN playerCharacters.dexterity<14 THEN 1\n" +
                        "                ELSE 2\n" +
                        "             END\n" +
                        "            FROM playerCharacters WHERE playerCharacters.id=new.ownerID)\n" +
                        "     WHEN armor.type='Heavy'\n" +
                        "    \tTHEN (SELECT 0)\n" +
                        "        \n" +
                        "END\n" +
                        "FROM armor INNER JOIN ownedItem ON armor.name=NEW.name)\n" +
                        "WHERE playerCharacters.id=NEW.ownerID;\n" +
                        "END;"
        );
    }

    private static void createEquippedWeaponTrigger(AppDatabase instance) {
        instance.getOpenHelper().getWritableDatabase().execSQL(
                "CREATE TRIGGER update_attack_die AFTER UPDATE ON ownedItem\n" +
                        "WHEN NEW.type = 'weapon' AND NEW.equipped=1\n" +
                        "BEGIN\n" +
                        "UPDATE playerCharacters\n" +
                        "\tSET attackdie=(SELECT damage FROM weapons INNER JOIN ownedItem ON weapons.name=NEW.name)\n" +
                        "\tWHERE playerCharacters.id=NEW.ownerID;\n" +
                        "END;"
        );
    }

    private static void populateDatabase(AppDatabase instance) {
        instance.weaponDao().insertAll(WeaponData.populateWeaponData());
        instance.armorDao().insertAll(ArmorData.populateArmorData());
        instance.RaceDao().insertAll(RaceData.populateRaceData());
        instance.toolDao().insertAll(ToolData.populateToolData());
        instance.backgroundDao().insertAll(BackgroundData.populateBackgroundData());
        instance.classDao().insertAll(ClassData.populateClassData());
        instance.ammunitionDao().insertAll(AmmunitionData.populateAmmunitionData());
        instance.shieldDao().insertAll(ShieldData.populateShieldData());
    }

    public abstract ArmorDao armorDao();
    public abstract RaceDao RaceDao();
    public abstract ToolDao toolDao();
    public abstract BackgroundDao backgroundDao();
    public abstract ClassDao classDao();
    public abstract AmmunitionDao ammunitionDao();
    public abstract PlayerCharacterDao playerCharacterDao();
    public abstract WeaponDao weaponDao();
    public abstract OwnedItemDao ownedItemDao();
    public abstract CustomItemDao customItemDao();
    public abstract ShieldDao shieldDao();
}
