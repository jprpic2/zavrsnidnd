package com.example.dndbeginner.Database.Room.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dndbeginner.Database.Room.Data.IBackpackItem;
import com.example.dndbeginner.Database.Room.Data.IOffhandItem;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.Weapon.WeaponEntry;
import com.example.dndbeginner.R;

import org.w3c.dom.Text;

import java.util.List;

public class OffhandItemAdapter extends RecyclerView.Adapter<OffhandItemAdapter.OffhandItemViewHolder> {


    private List<IOffhandItem> offhandItems;
    private List<OwnedItemEntry> ownedItems;
    private Context context;
    final private OffhandItemClickListener mOffhandItemClickListner;



    public OffhandItemAdapter(Context context, OffhandItemClickListener listener){
        this.context=context;
        this.mOffhandItemClickListner = listener;
    }


    @NonNull
    @Override
    public OffhandItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.offhand_item,parent,false);
        return new OffhandItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OffhandItemViewHolder holder, int position) {
        IOffhandItem offhandItem = offhandItems.get(position);
        OwnedItemEntry ownedItemEntry= ownedItems.get(position);
        if(offhandItem!=null){
            holder.itemName.setText(offhandItem.getItemName());
            holder.itemProperty.setText(offhandItem.getItemProperty());
        }
        if(ownedItemEntry!=null){
            if(ownedItemEntry.isOffhand()){
                holder.itemEquipped.setVisibility(View.VISIBLE);
            }
            else {
                holder.itemEquipped.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return offhandItems.size();
    }

    public List<IOffhandItem> getOffhandItems() {
        return offhandItems;
    }

    public void setOffhandItems(List<IOffhandItem> offhandItems) {
        this.offhandItems = offhandItems;
        notifyDataSetChanged();
    }

    public void setOwnedItems(List<OwnedItemEntry> ownedItems){
        this.ownedItems=ownedItems;
    }

    public interface OffhandItemClickListener {
        void onOffhandClick(int position);
    }

    class OffhandItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView itemName;
        TextView itemProperty;
        TextView itemEquipped;

        public OffhandItemViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName=itemView.findViewById(R.id.offhand_item_name);
            itemProperty=itemView.findViewById(R.id.offhand_item_property);
            itemEquipped=itemView.findViewById(R.id.offhand_item_equipped_label);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOffhandItemClickListner.onOffhandClick(getAdapterPosition());
        }
    }
}
