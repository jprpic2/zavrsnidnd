package com.example.dndbeginner.Database.Room.Data;

public interface IBackpackItem {
    String getItemName();
    String getItemDescription();
}
