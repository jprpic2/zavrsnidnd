package com.example.dndbeginner.Database.Room.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.Weapon.WeaponEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;
import com.example.dndbeginner.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class WeaponAdapter extends RecyclerView.Adapter<WeaponAdapter.WeaponViewHolder> {

    private List<WeaponEntry> weaponEntries;
    private List<OwnedItemEntry> ownedWeapons;
    private Context context;
    final private WeaponClickListener mWeaponClickListener;

    public WeaponAdapter(Context context, WeaponClickListener listener){
        this.context=context;
        this.mWeaponClickListener = listener;
    }

    @NonNull
    @Override
    public WeaponViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.weapon_item,parent,false);
        return new WeaponViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final WeaponViewHolder holder, int position) {
        WeaponEntry weaponEntry = weaponEntries.get(position);
        OwnedItemEntry ownedWeapon=ownedWeapons.get(position);
        if(ownedWeapon.isEquipped()){
            holder.weaponEquipped.setVisibility(View.VISIBLE);
        }
        else {
            holder.weaponEquipped.setVisibility(View.GONE);
        }
        if(weaponEntry!=null){
            holder.weaponName.setText(weaponEntry.getName());
            holder.weaponDamageType.setText(weaponEntry.getDamageType());
            holder.weaponDamage.setText(weaponEntry.getDamage());
            holder.weaponType.setText(weaponEntry.getType());
            if(StringListConverter.toString(weaponEntry.getProperties()).isEmpty()){
                holder.weaponProperties.setText(R.string.empty_list_none);
            }
            else{
                holder.weaponProperties.setText(StringListConverter.toString(weaponEntry.getProperties()));
            }
            holder.weaponRange.setText(weaponEntry.getRange());
        }
    }
    @Override
    public int getItemCount() {
        return weaponEntries.size();
    }

    public void setWeaponEntries(List<WeaponEntry> weaponEntries){
        this.weaponEntries=weaponEntries;
        notifyDataSetChanged();
    }

    public void setOwnedWeapons(List<OwnedItemEntry> ownedWeapons){
        this.ownedWeapons=ownedWeapons;
    }
    public List<WeaponEntry> getWeapons() {
        return this.weaponEntries;
    }

    public interface WeaponClickListener {
        void onWeaponClickListener(int position);
    }

    class WeaponViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView weaponName;
        TextView weaponType;
        TextView weaponDamage;
        TextView weaponDamageType;
        TextView weaponProperties;
        TextView weaponRange;
        TextView weaponEquipped;

        public WeaponViewHolder(@NonNull View itemView) {
            super(itemView);
            weaponName=itemView.findViewById(R.id.weapon_name);
            weaponType=itemView.findViewById(R.id.weapon_type);
            weaponDamage=itemView.findViewById(R.id.weapon_damage);
            weaponDamageType=itemView.findViewById(R.id.weapon_damage_type);
            weaponProperties=itemView.findViewById(R.id.weapon_properties);
            weaponRange=itemView.findViewById(R.id.weapon_range);
            weaponEquipped=itemView.findViewById(R.id.weapon_equipped_label);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mWeaponClickListener.onWeaponClickListener(getAdapterPosition());
        }
    }
}
