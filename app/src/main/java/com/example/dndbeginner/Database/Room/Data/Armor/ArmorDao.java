package com.example.dndbeginner.Database.Room.Data.Armor;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.dndbeginner.Database.Room.Data.BaseDao;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolEntry;

import java.util.List;

@Dao
public interface ArmorDao extends BaseDao<ArmorEntry> {

    @Query("SELECT * FROM armor")
    LiveData<List<ArmorEntry>> loadAllArmor();

    @Query("SELECT name FROM armor")
    LiveData<List<String>> loadArmorNames();

    @Query("SELECT * from armor WHERE name IN (:names)")
    LiveData<List<ArmorEntry>> getOwnedArmor(List<String> names);


    @Query("SELECT name FROM armor")
    List<String> getArmorNames();

    @Query("SELECT * FROM armor WHERE name = :name")
    ArmorEntry findArmor(String name);
}

