package com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.Room.Data.CustomItem.CustomItemEntry;
import com.example.dndbeginner.Database.Room.Data.IBackpackItem;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolEntry;

import java.util.List;

public class PlayerOwnedItemsViewModel extends AndroidViewModel {

    private static final String TAG = PlayerOwnedItemsViewModel.class.getSimpleName();

    private final AppDatabase database;

    public PlayerOwnedItemsViewModel(@NonNull Application application) {
        super(application);
        database = AppDatabase.getInstance(this.getApplication());
    }

    public LiveData<List<OwnedItemEntry>> getOwnedWeapons(int ownerID){
        return database.ownedItemDao().getOwnedWeapons(ownerID);
    }

    public LiveData<List<OwnedItemEntry>> getOwnedArmor(int ownerID){
        return database.ownedItemDao().getOwnedArmor(ownerID);
    }

    public LiveData<List<OwnedItemEntry>> getOwnedTools(int ownerID){
        return database.ownedItemDao().getOwnedTools(ownerID);
    }

    public LiveData<List<OwnedItemEntry>> getOwnedShields(int ownerID){
        return database.ownedItemDao().getOwnedShields(ownerID);
    }

    public LiveData<List<CustomItemEntry>> getBackpackCustomItems(int ownerID){
        return database.ownedItemDao().getBackpackCustomItems(ownerID);
    }

    public LiveData<List<OwnedItemEntry>> getOwnedWeaponsAndShields(int ownerID) {
        return database.ownedItemDao().getOwnedWeaponsAndShields(ownerID);
    }
}