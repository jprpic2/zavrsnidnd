package com.example.dndbeginner.Database.Room.Data.Race;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Arrays;
import java.util.List;

@Entity(tableName = "race")
public class RaceEntry {
    @Nullable
    private String baseRace;
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "race_name")
    private String name;
    private String size;
    private Integer speed;
    @Nullable
    private List<String> skills;
    @NonNull
    private List<String> languages;
    private int bonusLanguage;
    @NonNull
    private List<String> abilityIncrease;

    public RaceEntry(@Nullable String baseRace, @NonNull String name, String size, Integer speed, @Nullable List<String> skills, @NonNull List<String> languages, int bonusLanguage, @NonNull List<String> abilityIncrease) {
        this.baseRace = baseRace;
        this.name = name;
        this.size = size;
        this.speed = speed;
        this.skills = skills;
        this.languages = languages;
        this.bonusLanguage = bonusLanguage;
        this.abilityIncrease = abilityIncrease;
    }
    @Ignore
    public RaceEntry() {
        this.baseRace = "Dragonborn";
        this.name = "Dragonborn";
        this.size = "Medium";
        this.speed = 30;
        this.skills = Arrays.asList("Draconic Ancestry", "Breath Weapon", "Damage Resistance");
        this.languages = Arrays.asList("Common", "Draconic");
        this.bonusLanguage = 0;
        this.abilityIncrease = Arrays.asList("+2 STR","+1 CHA");
    }

    @Nullable
    public String getBaseRace() {
        return baseRace;
    }

    public void setBaseRace(@Nullable String baseRace) {
        this.baseRace = baseRace;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    @Nullable
    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(@Nullable List<String> skills) {
        this.skills = skills;
    }

    @NonNull
    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(@NonNull List<String> languages) {
        this.languages = languages;
    }

    public int getBonusLanguage() {
        return bonusLanguage;
    }

    public void setBonusLanguage(int bonusLanguage) {
        this.bonusLanguage = bonusLanguage;
    }

    @NonNull
    public List<String> getAbilityIncrease() {
        return abilityIncrease;
    }

    public void setAbilityIncrease(@NonNull List<String> abilityIncrease) {
        this.abilityIncrease = abilityIncrease;
    }
}
