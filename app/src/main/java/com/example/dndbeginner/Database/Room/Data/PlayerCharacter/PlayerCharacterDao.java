package com.example.dndbeginner.Database.Room.Data.PlayerCharacter;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.dndbeginner.Database.Room.Data.BaseDao;
import com.example.dndbeginner.Database.Room.Data.Class.ClassEntry;
import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;

import java.util.List;

@Dao
public interface PlayerCharacterDao extends BaseDao<PlayerCharacterEntry> {

    @Query("SELECT * FROM playerCharacters")
    LiveData<List<PlayerCharacterEntry>> loadAllPlayerCharacters();

    @Query("SELECT * FROM playerCharacters WHERE id LIKE :id")
    LiveData<PlayerCharacterEntry> getPlayerCharacter(int id);

    @Query("SELECT * FROM playerCharacters WHERE id LIKE :id")
    PlayerCharacterEntry getStaticPlayerCharacter(int id);

    @Insert
    long insertWithID(PlayerCharacterEntry playerCharacterEntry);

}
