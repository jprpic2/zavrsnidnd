package com.example.dndbeginner.Database.Room.Data.Race;

import java.util.Arrays;
import java.util.Collections;

public class RaceData {
    public static RaceEntry[] populateRaceData(){
        return new RaceEntry[]{
                new RaceEntry("Dragonborn","Dragonborn","Medium",30, Arrays.asList("Draconic Ancestry", "Breath Weapon", "Damage Resistance"),Arrays.asList("Common", "Draconic"),0,Arrays.asList("+2 STR","+1 CHA")),
        new RaceEntry("Dwarf","Hill Dwarf","Medium",25, Arrays.asList("Darkvision", "Dwarven Resilience", "Dwarven Combat Training", "Tool Proficiency", "Stonecunning", "Dwarven Toughness"),Arrays.asList("Common", "Dwarvish"),0,Arrays.asList("+2 CON","+1 WIS")),
        new RaceEntry("Dwarf","Mountain Dwarf","Medium",25, Arrays.asList("Darkvision", "Dwarven Resilience", "Dwarven Combat Training", "Tool Proficiency", "Stonecunning", "Dwarven Armor Training"),Arrays.asList("Common", "Dwarvish"),0,Arrays.asList("+2 CON","+2 STR")),
        new RaceEntry("Elf","High Elf","Medium",30, Arrays.asList("Darkvision", "Keen Senses", "Fey Ancestry", "Trance", "Elf Weapon Training", "Cantrip"),Arrays.asList("Common", "Elvish"),1, Arrays.asList("+2 DEX","+1 INT")),
        new RaceEntry("Elf","Wood Elf","Medium",35, Arrays.asList("Darkvision", "Keen Senses", "Fey Ancestry", "Trance", "Elf Weapon Training", "Mask of the Wild"),Arrays.asList("Common", "Elvish"),0, Arrays.asList("+2 DEX","+1 WIS")),
        new RaceEntry("Gnome","Deep Gnome","Small",25, Arrays.asList("Superior Darkvision", "Gnome Cunning", "Stone Camouflage"),Arrays.asList("Common", "Gnomish","Undercommon"),0,Arrays.asList("+2 INT","+1 DEX")),
        new RaceEntry("Gnome","Rock Gnome","Small",25, Arrays.asList("Darkvision", "Gnome Cunning", "Artificer’s Lore", "Tinker"),Arrays.asList("Common", "Gnomish"),0,Arrays.asList("+2 INT","+1 CON")),
        new RaceEntry("Halfling","Lightfoot Halfling","Small",25, Arrays.asList("Lucky", "Brave", "Halfling Nimbleness", "Naturally Stealthy"), Arrays.asList("Common", "Halfling"),0,Arrays.asList("+2 DEX","+1 CHA")),
        new RaceEntry("Halfling","Stout Halfling","Small",25, Arrays.asList("Lucky", "Brave", "Halfling Nimbleness", "Stout Resilience"), Arrays.asList("Common", "Halfling"),0,Arrays.asList("+2 DEX","+1 CON")),
        new RaceEntry("Half-Elf","Half-Elf","Medium",30, Arrays.asList("Darkvision", "Fey Ancestry", "Skill Versaility"),Arrays.asList("Common", "Elvish"),1, Collections.singletonList("+2 CHA")),
        new RaceEntry("Half-Orc","Half-Orc","Medium",30, Arrays.asList("Darkvision", "Menacing", "Relentless Endurance", "Savage Attacks"), Arrays.asList("Common", "Orc"),0,Arrays.asList("+2 STR","+1 CON")),
        new RaceEntry("Human","Human","Medium",30,null, Collections.singletonList("Common"),1, Collections.singletonList("+1 ALL")),
        new RaceEntry("Tiefling","Tiefling","Medium",30, Arrays.asList("Darkvision", "Hellish Resistance", "Infernal Legacy"),Arrays.asList("Common", "Infernal"),0,Arrays.asList("+1 INT","+2 CHA"))
        };
    }
}
