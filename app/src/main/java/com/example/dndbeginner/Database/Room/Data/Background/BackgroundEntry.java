package com.example.dndbeginner.Database.Room.Data.Background;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Arrays;
import java.util.List;

@Entity(tableName = "background")
public class BackgroundEntry {
    @NonNull
    @PrimaryKey
    private String name;
    @NonNull
    private List<String> skills;
    @Nullable
    private List<String> tools;
    private int bonusLanguages;
    private int numberOfTools;
    private int numberOfMusic;

    public BackgroundEntry(@NonNull String name, @NonNull List<String> skills, @Nullable List<String> tools, int bonusLanguages, int numberOfTools, int numberOfMusic) {
        this.name = name;
        this.skills = skills;
        this.tools = tools;
        this.bonusLanguages = bonusLanguages;
        this.numberOfTools = numberOfTools;
        this.numberOfMusic = numberOfMusic;
    }
    @Ignore
    public BackgroundEntry(){
        this.name="Acolyte";
        this.skills=Arrays.asList("Insight", "Religion");
        this.tools=null;
        this.bonusLanguages=2;
        this.numberOfTools=0;
        this.numberOfMusic=0;
    }
    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(@NonNull List<String> skills) {
        this.skills = skills;
    }

    @Nullable
    public List<String> getTools() {
        return tools;
    }

    public void setTools(@Nullable List<String> tools) {
        this.tools = tools;
    }

    public int getBonusLanguages() {
        return bonusLanguages;
    }

    public void setBonusLanguages(int bonusLanguages) {
        this.bonusLanguages = bonusLanguages;
    }

    public int getNumberOfTools() {
        return numberOfTools;
    }

    public void setNumberOfTools(int numberOfTools) {
        this.numberOfTools = numberOfTools;
    }

    public int getNumberOfMusic() {
        return numberOfMusic;
    }

    public void setNumberOfMusic(int numberOfMusic) {
        this.numberOfMusic = numberOfMusic;
    }
}
