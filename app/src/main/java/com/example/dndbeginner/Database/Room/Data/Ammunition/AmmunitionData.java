package com.example.dndbeginner.Database.Room.Data.Ammunition;

public class AmmunitionData {
    public static AmmunitionEntry[] populateAmmunitionData(){
        return new AmmunitionEntry[]{
                new AmmunitionEntry ("Arrows",20,100,1),
        new AmmunitionEntry ("Blowgun needles",50,100,1),
        new AmmunitionEntry ("Crossbow bolts",20,100,1),
        new AmmunitionEntry ("Sling bullets",20,4,1),
        };
    }
}
