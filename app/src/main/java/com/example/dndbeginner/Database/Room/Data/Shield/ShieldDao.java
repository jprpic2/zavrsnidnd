package com.example.dndbeginner.Database.Room.Data.Shield;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.example.dndbeginner.Database.Room.Data.Armor.ArmorEntry;
import com.example.dndbeginner.Database.Room.Data.BaseDao;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolEntry;

import java.util.List;

@Dao
public interface ShieldDao extends BaseDao<ShieldEntry> {

    @Query("SELECT * FROM shield")
    List<ShieldEntry> getShields();

    @Query("SELECT name FROM shield")
    List<String> getShieldNames();

    @Query("SELECT * from shield WHERE name IN (:names)")
    List<ShieldEntry> getShieldsFromNames(List<String> names);
}
