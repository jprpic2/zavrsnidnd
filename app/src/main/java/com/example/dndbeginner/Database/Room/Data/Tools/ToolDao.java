package com.example.dndbeginner.Database.Room.Data.Tools;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.dndbeginner.Database.Room.Data.BaseDao;

import java.util.List;

@Dao
public interface ToolDao extends BaseDao<ToolEntry> {

    @Query("SELECT * FROM tools")
    LiveData<List<ToolEntry>> loadAllTools();

    @Query("SELECT name FROM tools WHERE type LIKE 'Artisan Tools'")
    LiveData<List<String>> loadToolsNames();

    @Query("SELECT name FROM tools WHERE type LIKE 'Musical instrument'")
    LiveData<List<String>> loadMusicNames();

    @Query("SELECT name FROM tools")
    LiveData<List<String>> loadAllToolNames();

    @Query("SELECT * from tools WHERE name IN (:names)")
    LiveData<List<ToolEntry>> getOwnedTools(List<String> names);

}
