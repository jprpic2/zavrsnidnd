package com.example.dndbeginner.Database.Room.Data.CustomItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.example.dndbeginner.Database.Room.Data.IBackpackItem;

@Entity(tableName = "custom_items")
public class CustomItemEntry implements IBackpackItem {
    @PrimaryKey(autoGenerate = true)
    int id;
    @NonNull
    private String name;
    @Nullable
    private String description;
    private int ownerID;


    public CustomItemEntry(int id, @NonNull String name, @Nullable String description, int ownerID) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.ownerID = ownerID;
    }

    @Ignore
    public CustomItemEntry(@NonNull String name, @Nullable String description, int ownerID) {
        this.name = name;
        this.description = description;
        this.ownerID = ownerID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    @Override
    public String getItemName() {
        return name;
    }

    @Override
    public String getItemDescription() {
        return description;
    }
}
