package com.example.dndbeginner.Database.Room.Data.CustomItem;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.example.dndbeginner.Database.Room.Data.BaseDao;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;

import java.util.List;

@Dao
public interface CustomItemDao extends BaseDao<CustomItemEntry> {

    @Query("SELECT * FROM custom_items where ownerID=:ownerID")
    LiveData<List<CustomItemEntry>> getCustomItems(int ownerID);
}
