package com.example.dndbeginner.Database.Room.Data.Background;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BackgroundData {
    public static BackgroundEntry[] populateBackgroundData(){
        return new BackgroundEntry[]{
                new BackgroundEntry ("Acolyte",Arrays.asList("Insight", "Religion"),null,2,0,0),
        new BackgroundEntry ("Charlatan",Arrays.asList("Deception", "Sleight of Hand"),Arrays.asList("Disguise kit", "Forgery kit"),0,0,0),
        new BackgroundEntry ("Criminal",Arrays.asList("Deception", "Stealth"), Arrays.asList("Gaming set", "Thieves’ tools"),0,0,0),
        new BackgroundEntry ("Entertainer",Arrays.asList("Acrobatics", "Performance"), Collections.singletonList("Disguise kit"),0,0,1),
        new BackgroundEntry ("Folk Hero",Arrays.asList("Animal Handling", "Survival"), Collections.singletonList("Vehicles(land)"),0,1,0),
        new BackgroundEntry ("Guild Artisan",Arrays.asList("Insight", "Persuasion"), null,1,1,0),
        new BackgroundEntry ("Hermit",Arrays.asList("Medicine", "Religion"), Collections.singletonList("Herbalism kit"),1,0,0),
        new BackgroundEntry ("Noble",Arrays.asList("History", "Persuasion"), Collections.singletonList("Gaming Set"),1,0,0),
        new BackgroundEntry ("Outlander",Arrays.asList("Athletics", "Survival"), null,1,0,1),
        new BackgroundEntry ("Sage",Arrays.asList("Arcana", "History"),null,2,0,0),
        new BackgroundEntry ("Sailor",Arrays.asList("Athletics", "Perception"),Arrays.asList("Navigator tools", "Vehicles(water)"),0,0,0),
        new BackgroundEntry ("Soldier",Arrays.asList("Athletics", "Intimidation"),Arrays.asList("Gaming set", "Vehicles(land)"),0,0,0),
        new BackgroundEntry ("Urchin",Arrays.asList("Sleight of Hand", "Stealth"), Arrays.asList("Disguise kit", "Thieves tools"), 0,0,0),
        };
    }
}
