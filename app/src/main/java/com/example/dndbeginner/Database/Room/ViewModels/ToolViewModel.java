package com.example.dndbeginner.Database.Room.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolEntry;

import java.util.List;

public class ToolViewModel extends AndroidViewModel {
    private static final String TAG = ToolViewModel.class.getSimpleName();

    private LiveData<List<ToolEntry>> tools;
    private AppDatabase database;


    public ToolViewModel(@NonNull Application application) {
        super(application);
        database = AppDatabase.getInstance(this.getApplication());
        tools = database.toolDao().loadAllTools();
    }

    public LiveData<List<ToolEntry>> getTools() {
        return tools;
    }

    public LiveData<List<String>> getToolNames(){
        return database.toolDao().loadToolsNames();
    }
    public LiveData<List<String>> getMusicNames(){return
            database.toolDao().loadMusicNames();
    }
    public LiveData<List<String>> getAllNames(){
        return database.toolDao().loadAllToolNames();
    }

    public LiveData<List<ToolEntry>> getOwnedTools(List<String> toolNames){
        return database.toolDao().getOwnedTools(toolNames);
    }
}
