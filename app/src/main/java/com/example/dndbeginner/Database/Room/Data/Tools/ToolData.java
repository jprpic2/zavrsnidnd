package com.example.dndbeginner.Database.Room.Data.Tools;

public class ToolData {
    public static ToolEntry[] populateToolData(){
        return new ToolEntry[]{
                new ToolEntry ("Alchemist's supplies","Artisan Tools",5000,8),
        new ToolEntry ("Brewer's supplies","Artisan Tools",2000,9),
        new ToolEntry ("Calligrapher's supplies","Artisan Tools",1000,5),
        new ToolEntry ("Carpenter's tools","Artisan Tools",800,6),
        new ToolEntry ("Cartographer's tools","Artisan Tools",1050,6),
        new ToolEntry ("Cobbler's tools","Artisan Tools",500,5),
        new ToolEntry ("Cook's utensils","Artisan Tools",100,8),
        new ToolEntry ("Glassblower's tools","Artisan Tools",3000,5),
        new ToolEntry ("Jeweler's tools","Artisan Tools",2500,2),
        new ToolEntry ("Leatherworker's tools","Artisan Tools",500,5),
        new ToolEntry ("Mason's tools","Artisan Tools",1000,8),
        new ToolEntry ("Painter's supplies","Artisan Tools",1000,5),
        new ToolEntry ("Potter's tools","Artisan Tools",1000,3),
        new ToolEntry ("Smith's tools","Artisan Tools",2000,8),
        new ToolEntry ("Tinker's tools","Artisan Tools",5000,10),
        new ToolEntry ("Weaver's tools","Artisan Tools",100,5),
        new ToolEntry ("Woodcarver's tools","Artisan Tools",100,5),

        new ToolEntry ("Dice set","Gaming set",10,0),
        new ToolEntry ("Dragonchess set","Gaming set",100,1),
        new ToolEntry ("Playing card set","Gaming set",50,0),
        new ToolEntry ("Three-Dragon Ante set","Gaming set",100,0),

        new ToolEntry ("Bagpipes","Musical instrument",3000,6),
        new ToolEntry ("Drum","Musical instrument",600,3),
        new ToolEntry ("Dulcimer","Musical instrument",2500,10),
        new ToolEntry ("Flute","Musical instrument",200,1),
        new ToolEntry ("Lute","Musical instrument",3500,2),
        new ToolEntry ("Lyre","Musical instrument",3000,2),
        new ToolEntry ("Horn","Musical instrument",300,2),
        new ToolEntry ("Pan flute","Musical instrument",1200,2),
        new ToolEntry ("Shawn","Musical instrument",200,1),
        new ToolEntry ("Viol","Musical instrument",3000,1),

        new ToolEntry ("Disguise kit","Other",2500,3),
        new ToolEntry ("Forgery kit","Other",1500,5),
        new ToolEntry ("Herbalism kit","Other",500,3),
        new ToolEntry ("Navigator's tools","Other",2500,2),
        new ToolEntry ("Poisoner's kit","Other",5000,2),
        new ToolEntry ("Thieve's tools","Other",2500,1),
        new ToolEntry ("Vehicles (land)","Other",0,0),
        new ToolEntry ("Vehicles (water)","Other",0,0),

                new ToolEntry("Burglar's Pack","Includes a backpack, a bag of 1,000 ball bearings, 10 feet of string, a bell, 5 candles, a crowbar, a hammer, 10 pitons, a hooded lantern, 2 flasks of oil, 5 days rations, a tinderbox, and a waterskin. The pack also has 50 feet of hempen rope strapped to the side of it.",16,0),
                new ToolEntry("Diplomat's Pack","Includes a chest, 2 cases for maps and scrolls, a set of fine clothes, a bottle of ink, an ink pen, a lamp, 2 flasks of oil, 5 sheets of paper, a vial of perfume, sealing wax, and soap.",39,0),
                new ToolEntry("Dungeoneer's Pack","Includes a backpack, a crowbar, a hammer, 10 pitons, 10 torches, a tinderbox, 10 days of rations, and a waterskin. The pack also has 50 feet of hempen rope strapped to the side of it.",12,0),
                new ToolEntry("Entertainer's Pack ","Includes a backpack, a bedroll, 2 costumes, 5 candles, 5 days of rations, a waterskin, and a disguise kit.",40,0),
                new ToolEntry("Explorer's Pack","Includes a backpack, a bedroll, a mess kit, a tinderbox, 10 torches, 10 days of rations, and a waterskin. The pack also has 50 feet of hempen rope strapped to the side of it.",10,0),
                new ToolEntry("Priest's Pack","Includes a backpack, a blanket, 10 candles, a tinderbox, an alms box, 2 blocks of incense, a censer, vestments, 2 days of rations, and a waterskin.",19,0),
                new ToolEntry("Scholar's Pack","Includes a backpack, a book of lore, a bottle of ink, an ink pen, 10 sheets of parchment, a little bag of sand, and a small knife.",40,0),

                new ToolEntry("Crystal","Arcane Focus",1000,1),
                new ToolEntry ("Orb","Arcane Focus",2000,3),
                new ToolEntry ("Rod","Arcane Focus",1000,2),
                new ToolEntry ("Staff","Arcane Focus",500,4),
                new ToolEntry ("Wand","Arcane Focus",1000,1),

                new ToolEntry ("Sprig of mistletoe","Druidic Focus",100,0),
                new ToolEntry ("Totem","Druidic Focus",100,0),
                new ToolEntry ("Wooden staff","Druidic Focus",500,4),
                new ToolEntry ("Yew wand","Druidic Focus",1000,1),

                new ToolEntry ("Amulet","Holy Symbol",500,1),
                new ToolEntry ("Emblem","Holy Symbol",500,0),
                new ToolEntry ("Reliquary","Holy Symbol",500,2)
        };
    }
}
