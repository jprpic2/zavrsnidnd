package com.example.dndbeginner.Database.Room.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.Room.Data.Weapon.WeaponEntry;

import java.util.List;

public class WeaponViewModel extends AndroidViewModel {
    private static final String TAG = WeaponViewModel.class.getSimpleName();

    private LiveData<List<WeaponEntry>> weaponEntries;

    AppDatabase database;


    public WeaponViewModel(@NonNull Application application) {
        super(application);
        database = AppDatabase.getInstance(this.getApplication());
        weaponEntries = database.weaponDao().getWeapons();
    }

    public LiveData<List<WeaponEntry>> getWeaponEntries() {
        return weaponEntries;
    }

    public LiveData<List<String>> getWeaponNames(){
        return database.weaponDao().loadWeaponNames();
    }

    public LiveData<List<WeaponEntry>> getOwnedWeapons(List<String> names){
        return database.weaponDao().getOwnedWeapons(names);
    }

    public LiveData<List<WeaponEntry>> getWeapons() {
        return database.weaponDao().getWeapons();
    }
}
