package com.example.dndbeginner.Database.Room.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.R;
import com.example.dndbeginner.Database.Room.Data.Armor.ArmorEntry;

import java.util.List;

public class ArmorAdapter extends RecyclerView.Adapter<ArmorAdapter.ArmorViewHolder> {

    private List<ArmorEntry> armorList;
    private List<OwnedItemEntry> ownedArmors;
    private final Context context;
    final private ArmorClickListener mArmorClickListener;


    public ArmorAdapter(Context context, ArmorClickListener mArmorClickListener){
        this.context = context;
        this.mArmorClickListener = mArmorClickListener;
    }

    @NonNull
    @Override
    public ArmorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.armor_item,parent,false);
        return new ArmorAdapter.ArmorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArmorViewHolder holder, int position) {
        ArmorEntry armor = armorList.get(position);
        OwnedItemEntry ownedArmor=ownedArmors.get(position);
        if(ownedArmor.isEquipped()){
            holder.armorEquipped.setVisibility(View.VISIBLE);
        }
        else{
            holder.armorEquipped.setVisibility(View.INVISIBLE);
        }
        if(armor!=null){
            holder.armorName.setText(armor.getName());
            holder.armorType.setText(armor.getType());
            holder.armorAC.setText(String.valueOf(armor.getArmorClass()));
            holder.armorStrengthRequired.setText(String.valueOf(armor.getStrengthRequired()));
            if(armor.getStrengthRequired()==0){
                holder.armorSTRLabel.setVisibility(View.GONE);
                holder.armorStrengthRequired.setVisibility(View.GONE);
            }
            else{
                holder.armorSTRLabel.setVisibility(View.VISIBLE);
                holder.armorStrengthRequired.setVisibility(View.VISIBLE);
            }
            if(armor.isStealthDisadvantage()){
                holder.armorStealthDisadvantage.setVisibility(View.VISIBLE);
            }
            else{
                holder.armorStealthDisadvantage.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return armorList.size();
    }

    public void setArmorList(List<ArmorEntry> armorList){
        this.armorList=armorList;
        notifyDataSetChanged();
    }

    public void setOwnedArmors(List<OwnedItemEntry> armorList){
        this.ownedArmors=armorList;
    }

    public List<ArmorEntry> getArmor() {
        return this.armorList;
    }

    public interface ArmorClickListener {
        void onArmorClickListener(int position);
    }

    class ArmorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView armorName;
        TextView armorType;
        TextView armorAC;
        TextView armorStrengthRequired;
        TextView armorSTRLabel;
        TextView armorStealthDisadvantage;
        TextView armorEquipped;


        public ArmorViewHolder(@NonNull View itemView) {
            super(itemView);
            armorName=itemView.findViewById(R.id.armor_name);
            armorType=itemView.findViewById(R.id.armor_type);
            armorAC=itemView.findViewById(R.id.armor_class);
            armorStrengthRequired=itemView.findViewById(R.id.armor_strength);
            armorSTRLabel=itemView.findViewById(R.id.armor_strength_label);
            armorStealthDisadvantage=itemView.findViewById(R.id.armor_stealth);
            armorEquipped=itemView.findViewById(R.id.armor_equipped_label);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mArmorClickListener.onArmorClickListener(getAdapterPosition());
        }
    }
}
