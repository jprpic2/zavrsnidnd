package com.example.dndbeginner.Database.Room.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.R;
import java.util.Arrays;
import java.util.List;

public class PlayerCharacterAdapter extends RecyclerView.Adapter<PlayerCharacterAdapter.PlayerCharacterViewHolder> {

    private List<PlayerCharacterEntry> playerCharacters;
    final private ItemClickListener mItemClickListener;
    private Context mContext;


    public PlayerCharacterAdapter(Context context, ItemClickListener listener) {
        mContext = context;
        mItemClickListener = listener;
    }

    @NonNull
    @Override
    public PlayerCharacterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext)
                .inflate(R.layout.player_character_entry, parent, false);
        return new PlayerCharacterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerCharacterViewHolder holder, int position) {
        PlayerCharacterEntry playerCharacterEntry = playerCharacters.get(position);
        List<Integer> experienceNeeded = Arrays.asList(300,900,2700,6500,14000,23000,34000,48000,64000,85000,100000,120000,140000,165000,190000,225000,265000,305000,355000,500000);
        if(playerCharacterEntry!=null){
            holder.playerCharacterName.setText(playerCharacterEntry.getName());
            holder.playerCharacterClass.setText(playerCharacterEntry.getCharacterClass());
            holder.playerCharacterLevel.setText(String.valueOf(playerCharacterEntry.getCharacterLevel()));
            holder.playerCharacterRace.setText(playerCharacterEntry.getRace());
            holder.playerCharacterBackground.setText(playerCharacterEntry.getBackground());
            holder.playerExperience.setText(String.valueOf(playerCharacterEntry.getExperience()));
            holder.nextLevelExperience.setText(String.valueOf(experienceNeeded.get(playerCharacterEntry.getCharacterLevel()-1)));
        }
    }

    @Override
    public int getItemCount() {
        if (playerCharacters == null) {
            return 0;
        }
        return playerCharacters.size();
    }

    public void setPlayerCharacters(List<PlayerCharacterEntry> playerCharacters){
        this.playerCharacters=playerCharacters;
        notifyDataSetChanged();
    }

    public List<PlayerCharacterEntry> getPlayerCharacters() {
        return this.playerCharacters;
    }

    public interface ItemClickListener {
        void onItemClickListener(int itemId);
    }

    class PlayerCharacterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView playerCharacterName;
        TextView playerCharacterClass;
        TextView playerCharacterLevel;
        TextView playerCharacterRace;
        TextView playerCharacterBackground;
        TextView playerExperience;
        TextView nextLevelExperience;

        public PlayerCharacterViewHolder(@NonNull View itemView) {
            super(itemView);
            playerCharacterName=itemView.findViewById(R.id.player_character_name);
            playerCharacterClass=itemView.findViewById(R.id.player_character_class);
            playerCharacterLevel=itemView.findViewById(R.id.player_character_level);
            playerCharacterRace=itemView.findViewById(R.id.player_character_race);
            playerCharacterBackground=itemView.findViewById(R.id.player_character_background);
            playerExperience=itemView.findViewById(R.id.player_character_player_experience);
            nextLevelExperience=itemView.findViewById(R.id.player_character_experience_next_level);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int elementId = playerCharacters.get(getAdapterPosition()).getId();
            mItemClickListener.onItemClickListener(elementId);
        }
    }
}
