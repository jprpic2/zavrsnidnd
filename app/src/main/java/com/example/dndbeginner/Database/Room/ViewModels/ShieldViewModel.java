package com.example.dndbeginner.Database.Room.ViewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.CustomItem.CustomItemEntry;
import com.example.dndbeginner.Database.Room.Data.Shield.ShieldEntry;

import java.util.ArrayList;
import java.util.List;

public class ShieldViewModel extends AndroidViewModel {

    List<ShieldEntry> shields;
    AppDatabase database;

    public ShieldViewModel(@NonNull Application application) {
        super(application);
        database=AppDatabase.getInstance(this.getApplication());
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                shields=database.shieldDao().getShields();
            }
        });
    }

    public List<ShieldEntry> getShields(){
        return shields;
    }

    public List<String> getShieldNames(){
        List<String> shieldNames=new ArrayList<>();
        for(ShieldEntry shield : shields){
            shieldNames.add(shield.getName());
        }
        return shieldNames;
    }

}
