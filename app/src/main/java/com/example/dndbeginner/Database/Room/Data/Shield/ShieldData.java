package com.example.dndbeginner.Database.Room.Data.Shield;

import com.example.dndbeginner.Database.Room.Data.Armor.ArmorEntry;

public class ShieldData {
    public static ShieldEntry[] populateShieldData(){
        return new ShieldEntry[]{
                new ShieldEntry("Shield",10,6),
                new ShieldEntry("Wooden Shield",10,6)
        };
    }
}
