package com.example.dndbeginner.Database.Room.TypeConverters;

import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class StringListConverter {
    @TypeConverter
    public static List<String> toList(String string){
        return string == null ? null : Arrays.asList(string.split("\\s*,\\s*"));
    }

    @TypeConverter
    public static String toString(List<String> stringList){
        if(stringList==null){
            return "";
        }
        String SEPARATOR = ", ";
        StringBuilder stringBuilder = new StringBuilder();
        for(String string : stringList){
            stringBuilder.append(string);
            stringBuilder.append(SEPARATOR);
        }
        String returnString=stringBuilder.toString();
        returnString = returnString.substring(0, returnString.length() - SEPARATOR.length());
        return returnString;
    }
}
