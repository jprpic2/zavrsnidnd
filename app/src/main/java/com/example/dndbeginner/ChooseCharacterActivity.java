package com.example.dndbeginner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.adapters.PlayerCharacterAdapter;
import com.example.dndbeginner.Fragments.Character.AddCharacter.AddCharacterActivity;
import com.example.dndbeginner.Fragments.CharacterActivity;
import com.example.dndbeginner.Fragments.Inventory.ItemsActivities.BackpackActivity;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import java.util.List;

public class ChooseCharacterActivity extends AppCompatActivity implements PlayerCharacterAdapter.ItemClickListener {

    private RecyclerView mCharacterRecycler;
    private TextView emptyTextView;
    private ExtendedFloatingActionButton fabAddCharacter;
    private PlayerCharacterAdapter mPlayerCharacterAdapter;
    private AppDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_selection);
        InitializeData();
        populateAdapter();

        fabAddCharacter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addTaskIntent = new Intent(ChooseCharacterActivity.this, AddCharacterActivity.class);
                startActivity(addTaskIntent);
            }
        });

        setUpSlideToDelete();
    }

    private void InitializeData() {
        emptyTextView = findViewById(R.id.empty_character_textview);
        mCharacterRecycler = findViewById(R.id.character_selection_recycler);
        fabAddCharacter = findViewById(R.id.fab_create_character);
        mDb = AppDatabase.getInstance(getApplicationContext());

        mPlayerCharacterAdapter = new PlayerCharacterAdapter(this, this);
        mCharacterRecycler.setLayoutManager(new LinearLayoutManager(this));
        mCharacterRecycler.setHasFixedSize(true);
        mCharacterRecycler.setAdapter(mPlayerCharacterAdapter);
    }


    private void populateAdapter() {
        mDb.playerCharacterDao().loadAllPlayerCharacters().observe(this, new Observer<List<PlayerCharacterEntry>>() {
            @Override
            public void onChanged(final List<PlayerCharacterEntry> playerCharacterEntries) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateRecycler(playerCharacterEntries);
                        mPlayerCharacterAdapter.setPlayerCharacters(playerCharacterEntries);
                    }
                });
            }
        });
    }

    private void updateRecycler(List<PlayerCharacterEntry> playerCharacterEntries){
        if(playerCharacterEntries.size()>0){
            mCharacterRecycler.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.INVISIBLE);
        }
        else{
            mCharacterRecycler.setVisibility(View.INVISIBLE);
            emptyTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemClickListener(int characterID) {
        Intent intent = new Intent(ChooseCharacterActivity.this, CharacterActivity.class);
        intent.putExtra(CharacterActivity.CHARACTER_ID, characterID);
        startActivity(intent);
    }

    private void setUpSlideToDelete() {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ChooseCharacterActivity.this,R.style.deleteCharacterAlertDialog);
                LayoutInflater inflater = ChooseCharacterActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.delete_character_alertdialog, null,false);
                alert.setView(dialogView).
                        setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                final int position = viewHolder.getAdapterPosition();
                                final List<PlayerCharacterEntry> shownCharacters=mPlayerCharacterAdapter.getPlayerCharacters();
                                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        mDb.playerCharacterDao().delete(shownCharacters.get(position));
                                    }
                                });
                            }})
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mPlayerCharacterAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                                dialog.dismiss();
                            }
                        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        mPlayerCharacterAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                        dialog.dismiss();
                    }
                });
                final AlertDialog mDialog = alert.create();
                mDialog.setOnShowListener( new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        mDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getColor(R.color.colorPrimaryDark));
                        mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getColor(R.color.colorPrimaryDark));
                    }
                });
                mDialog.show();
            }
        }).attachToRecyclerView(mCharacterRecycler);
    }
}