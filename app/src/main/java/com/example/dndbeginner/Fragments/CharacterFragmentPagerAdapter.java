package com.example.dndbeginner.Fragments;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.dndbeginner.Fragments.Character.CharacterFragment;
import com.example.dndbeginner.Fragments.Dice.DiceFragment;
import com.example.dndbeginner.Fragments.Inventory.InventoryFragment;
import com.example.dndbeginner.R;

public class CharacterFragmentPagerAdapter extends FragmentPagerAdapter {

    private static final int PAGE_COUNT = 3;
    private Context context;

    public CharacterFragmentPagerAdapter(@NonNull FragmentManager fm, int behavior,Context context) {
        super(fm, behavior);
        this.context=context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new DiceFragment();
        } else if (position == 1){
            return new CharacterFragment();
        } else {
            return new InventoryFragment();
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return context.getString(R.string.fragment_roll);
        } else if (position == 1){
            return context.getString(R.string.fragment_character);
        } else {
            return context.getString(R.string.fragment_inventory);
    }
}}
