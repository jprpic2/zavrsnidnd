package com.example.dndbeginner.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter.PlayerCharacterViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter.PlayerCharacterViewModelFactory;
import com.example.dndbeginner.R;
import com.google.android.material.tabs.TabLayout;

public class CharacterActivity extends AppCompatActivity {
    public static final String CHARACTER_ID = "extraTaskId";
    public static final String INSTANCE_TASK_ID = "instanceTaskId";

    private static final int DEFAULT_CHARACTER_ID = -1;
    private int mCharacterID = DEFAULT_CHARACTER_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content of the activity to use the activity_main.xml layout file
        setContentView(R.layout.activity_main);

        // Find the view pager that will allow the user to swipe between fragments
        ViewPager viewPager = findViewById(R.id.viewpager);

        // Create an adapter that knows which fragment should be shown on each page
        CharacterFragmentPagerAdapter adapter = new CharacterFragmentPagerAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,this);

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tab_layout);

        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);
        AppDatabase mDb = AppDatabase.getInstance(getApplicationContext());

        if (savedInstanceState != null && savedInstanceState.containsKey(INSTANCE_TASK_ID)) {
            mCharacterID = savedInstanceState.getInt(INSTANCE_TASK_ID, DEFAULT_CHARACTER_ID);
        }

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(CHARACTER_ID) || mCharacterID!=DEFAULT_CHARACTER_ID) {
            if (mCharacterID == DEFAULT_CHARACTER_ID) {
                // populate the UI
                mCharacterID = intent.getIntExtra(CHARACTER_ID, DEFAULT_CHARACTER_ID);
                PlayerCharacterViewModelFactory factory = new PlayerCharacterViewModelFactory(mDb, mCharacterID);
                new ViewModelProvider(this, factory).get(PlayerCharacterViewModel.class);
            }
            else{
                PlayerCharacterViewModelFactory factory = new PlayerCharacterViewModelFactory(mDb, mCharacterID);
                new ViewModelProvider(this, factory).get(PlayerCharacterViewModel.class);
            }
        }

}

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt(INSTANCE_TASK_ID, mCharacterID);
        super.onSaveInstanceState(outState);
    }
}
