package com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dndbeginner.Database.Room.Data.Background.BackgroundEntry;

public class CharacterBackgroundViewModel extends ViewModel {
    private MutableLiveData<BackgroundEntry> characterBackground= new MutableLiveData<>(new BackgroundEntry());

    public void setCharacterBackground(BackgroundEntry playerBackground) {
        this.characterBackground.setValue(playerBackground);
    }

    public LiveData<BackgroundEntry> getCharacterBackground(){
        return this.characterBackground;
    }
}
