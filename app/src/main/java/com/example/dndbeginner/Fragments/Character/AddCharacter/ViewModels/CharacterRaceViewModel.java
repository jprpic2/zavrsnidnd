package com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;

public class CharacterRaceViewModel extends ViewModel {
    private MutableLiveData<RaceEntry> characterRace=new MutableLiveData<>(new RaceEntry());

    public void setCharacterRace(RaceEntry playerRace){
        this.characterRace.setValue(playerRace);
    }

    public LiveData<RaceEntry> getCharacterRace(){
        return this.characterRace;
    }
}
