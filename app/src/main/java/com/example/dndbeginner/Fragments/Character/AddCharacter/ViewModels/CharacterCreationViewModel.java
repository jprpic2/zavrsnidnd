package com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dndbeginner.Database.Room.Data.Class.ClassEntry;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;

import java.util.List;
import java.util.Objects;

public class CharacterCreationViewModel extends ViewModel {
    private MutableLiveData<PlayerCharacterEntry> playerCharacter = new MutableLiveData<>(new PlayerCharacterEntry());

    public LiveData<PlayerCharacterEntry> getPlayerCharacter(){
        return playerCharacter;
    }

    public void setCharacterSkills(List<String> skills){
        Objects.requireNonNull(this.playerCharacter.getValue()).setSkills(skills);
    }
    public void setCharacterTools(List<String> tools){
        Objects.requireNonNull(this.playerCharacter.getValue()).setTools(tools);
    }
    public void setCharacterLanguages(List<String> languages){
        Objects.requireNonNull(this.playerCharacter.getValue()).setLanguages(languages);
    }
    public void setCharacterName(String name){
        Objects.requireNonNull(this.playerCharacter.getValue()).setName(name);
    }

    public void setPlayerSpells(List<String> spells){
        Objects.requireNonNull(this.playerCharacter.getValue()).setSpells(spells);
    }

    public void setCharacterClass(String className){
        Objects.requireNonNull(this.playerCharacter.getValue()).setCharacterClass(className);
    }
    public void setCharacterRace(String raceName){
        Objects.requireNonNull(this.playerCharacter.getValue()).setRace(raceName);
    }
    public void setCharacterBackground(String backgroundName){
        Objects.requireNonNull(this.playerCharacter.getValue()).setBackground(backgroundName);
    }

    public void setCharacterHitPoints(int hitPoints){
        Objects.requireNonNull(this.playerCharacter.getValue()).setHitPoints(hitPoints);
    }
}
