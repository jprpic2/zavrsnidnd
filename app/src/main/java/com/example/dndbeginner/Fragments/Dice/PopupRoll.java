package com.example.dndbeginner.Fragments.Dice;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.dndbeginner.R;

public class PopupRoll {

    private String diceRolled;
    private int rollResultSum;
    private String diceResults;

    public PopupRoll(String rollDice, int rollSum, String rollResults){
        this.diceRolled=rollDice;
        this.rollResultSum=rollSum;
        this.diceResults=rollResults;
    }

    //PopupWindow display method

    public void showPopupWindow(final View view) {


        view.getContext();
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_roll,null);

        int width =LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Initialize the elements of our window, install the handler

        TextView rollDice = popupView.findViewById(R.id.rollDice);
        TextView rollSum = popupView.findViewById(R.id.rollSum);
        TextView rollResults = popupView.findViewById(R.id.rollResults);

        rollDice.setText(diceRolled);
        rollSum.setText(String.valueOf(rollResultSum));
        rollResults.setText(diceResults);

        //Handler for clicking on the inactive zone of the window

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Close the window when clicked
                popupWindow.dismiss();
                return true;
            }
        });
    }
    }
