package com.example.dndbeginner.Fragments.Inventory.ItemsActivities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.Armor.ArmorEntry;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;
import com.example.dndbeginner.Database.Room.ViewModels.ArmorViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter.PlayerOwnedItemsViewModel;
import com.example.dndbeginner.Database.Room.adapters.ArmorAdapter;
import com.example.dndbeginner.Fragments.Character.CharacterFragment;
import com.example.dndbeginner.Fragments.Inventory.InventoryFragment;
import com.example.dndbeginner.R;

import java.util.ArrayList;
import java.util.List;

public class ArmorActivity extends AppCompatActivity implements ArmorAdapter.ArmorClickListener {

    private RecyclerView mRecyclerView;
    private AppDatabase mDb;
    private EditText addItemEditText;
    int playerCharacterID;
    private List<OwnedItemEntry> ownedItems;
    private ArmorAdapter mArmorAdapter;
    private ListView autoCompleteList;
    private List<String> autoCompleteArmor;
    private TextWatcher textWatcher;
    private FrameLayout autoCompleteFrameLayout;
    private ArmorViewModel armorViewModel;
    private View.OnClickListener addItemOnClickListener;
    private PlayerCharacterEntry playerCharacter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler);

        initializeData();
        setUpClickListener();

        final PlayerOwnedItemsViewModel playerOwnedItemsViewModel = new ViewModelProvider(this).get(PlayerOwnedItemsViewModel.class);
        armorViewModel = new ViewModelProvider(ArmorActivity.this).get(ArmorViewModel.class);

        setUpAutoComplete();

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                playerCharacter=mDb.playerCharacterDao().getStaticPlayerCharacter(playerCharacterID);
            }
        });
        playerOwnedItemsViewModel.getOwnedArmor(playerCharacterID).observe(ArmorActivity.this, new Observer<List<OwnedItemEntry>>() {
            @Override
            public void onChanged(List<OwnedItemEntry> ownedItemEntries) {
                final List<String> ownedArmorNames = new ArrayList<>();
                ownedItems = ownedItemEntries;
                for (OwnedItemEntry ownedItemEntry : ownedItemEntries) {
                    ownedArmorNames.add(ownedItemEntry.getName());
                }
                armorViewModel.getOwnedArmor(ownedArmorNames).observe(ArmorActivity.this, new Observer<List<ArmorEntry>>() {
                    @Override
                    public void onChanged(List<ArmorEntry> armorEntries) {
                        armorViewModel.getOwnedArmor(ownedArmorNames).removeObserver(this);
                        List<ArmorEntry> shownArmor = new ArrayList<>();
                        for (String ownedArmorName : ownedArmorNames) {
                            for (ArmorEntry armorEntry : armorEntries) {
                                if (ownedArmorName.equals(armorEntry.getName())) {
                                    shownArmor.add(armorEntry);
                                }
                            }
                        }
                        mArmorAdapter.setOwnedArmors(ownedItems);
                        mArmorAdapter.setArmorList(shownArmor);
                    }
                });
            }
        });


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        final int position = viewHolder.getAdapterPosition();
                        mDb.ownedItemDao().delete(ownedItems.get(position));
                    }
                });
            }
        }).attachToRecyclerView(mRecyclerView);

    }

    private void setUpClickListener() {
        addItemOnClickListener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredString = addItemEditText.getText().toString().trim();
                armorViewModel.getArmor().observe(ArmorActivity.this, new Observer<List<ArmorEntry>>() {
                    @Override
                    public void onChanged(final List<ArmorEntry> armorEntries) {
                        armorViewModel.getArmor().removeObserver(this);
                        AppExecutors.getInstance().diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                OwnedItemEntry newOwnedItem = new OwnedItemEntry(enteredString, "armor", playerCharacterID, 0,0);
                                mDb.ownedItemDao().insert(newOwnedItem);
                                ownedItems.add(newOwnedItem);
                            }
                        });
                    }
                });
                addItemEditText.setText("");
                addItemEditText.clearFocus();
            }
        };
    }

    private void setUpAutoComplete() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                autoCompleteArmor=mDb.armorDao().getArmorNames();
                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        final ArrayAdapter<String> arrayAdapter=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,autoCompleteArmor);
                        autoCompleteList.setAdapter(arrayAdapter);
                        textWatcher=new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                arrayAdapter.getFilter().filter(s);

                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if(s.length()==0){
                                    autoCompleteFrameLayout.setVisibility(View.GONE);
                                    addItemEditText.clearFocus();
                                }
                                else {
                                    autoCompleteFrameLayout.setVisibility(View.VISIBLE);
                                }
                            }
                        };
                        addItemEditText.addTextChangedListener(textWatcher);
                        autoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                String armorName=arrayAdapter.getItem(position);
                                addItemEditText.setText(armorName);
                                addItemEditText.setSelection(addItemEditText.getText().length());
                                addItemOnClickListener.onClick(getCurrentFocus());
                            }
                        });
                    }
                });

            }
        });
    }

    private void initializeData() {
        mDb = AppDatabase.getInstance(getApplicationContext());
        addItemEditText = findViewById(R.id.add_item_edittext);
        addItemEditText.setHint("Add an Armor");
        autoCompleteList=findViewById(R.id.autoCompleteList);
        mRecyclerView = findViewById(R.id.recycler);
        autoCompleteFrameLayout=findViewById(R.id.autoCompleteFrameLayout);
        mArmorAdapter=new ArmorAdapter(this,this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mArmorAdapter);

        mArmorAdapter.setArmorList(new ArrayList<ArmorEntry>());

        Intent intent = getIntent();
        playerCharacterID = intent.getIntExtra(InventoryFragment.EXTRA_INT, 1);
    }

    @Override
    public void onArmorClickListener(int position) {
        equipArmor(ownedItems.get(position));
    }

    private void equipArmor(final OwnedItemEntry ownedItemEntry) {
        Log.d("Name",ownedItemEntry.getName());
        if(ownedItemEntry.isEquipped()){
            return;
        }
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                ArmorEntry selectedArmor = mDb.armorDao().findArmor(ownedItemEntry.getName());
                if(selectedArmor.getStrengthRequired()<=playerCharacter.getStrength()){
                    for(final OwnedItemEntry ownedItem : ownedItems){
                        if(ownedItem.isEquipped()){
                            ownedItem.unEquip();
                            mDb.ownedItemDao().update(ownedItem);
                        }
                    }
                    ownedItemEntry.setEquipped();
                    mDb.ownedItemDao().update(ownedItemEntry);
                    updatePlayerCharacter(ownedItemEntry);
                }
                else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),"Insufficient strength", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

    }

    private void updatePlayerCharacter(OwnedItemEntry ownedItemEntry) {
        ArmorEntry equippedArmor=mDb.armorDao().findArmor(ownedItemEntry.getName());
        int playerDexterity=playerCharacter.getDexterity();
        int abilityModifier = Integer.parseInt(CharacterFragment.getAbilityModifier(playerDexterity));
        String armorType=equippedArmor.getType().toLowerCase();
        if(armorType.equals("light")){
            playerCharacter.setArmorClass(equippedArmor.getArmorClass()+abilityModifier);
        }
        else if(armorType.equals("medium")){
            if(abilityModifier>2){
                abilityModifier=2;
            }
            else if(abilityModifier<-2){
                abilityModifier=-2;
            }
            playerCharacter.setArmorClass(equippedArmor.getArmorClass()+abilityModifier);
        }
        else{
            if(equippedArmor.getStrengthRequired()>playerCharacter.getStrength()){
                Toast.makeText(getApplicationContext(),"Insufficient strength",Toast.LENGTH_SHORT).show();
            }
            else{
                playerCharacter.setArmorClass(equippedArmor.getArmorClass());
            }
        }

        mDb.playerCharacterDao().update(playerCharacter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        addItemEditText.removeTextChangedListener(textWatcher);
    }

}