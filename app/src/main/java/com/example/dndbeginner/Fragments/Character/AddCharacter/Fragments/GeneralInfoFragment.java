package com.example.dndbeginner.Fragments.Character.AddCharacter.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.Class.ClassEntry;
import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterClassViewModel;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterCreationViewModel;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterRaceViewModel;
import com.example.dndbeginner.R;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Objects;

public class GeneralInfoFragment extends Fragment {

    TextInputEditText characterName;
    NumberPicker baseRacePicker;
    NumberPicker subRacePicker;
    NumberPicker classPicker;
    TextView raceTextView;
    TextView raceTraits;
    TextView classTextView;
    TextView hitDieTextView;
    TextView primaryAbilitiesTextView;
    CharacterClassViewModel characterClassViewModel;
    CharacterRaceViewModel characterRaceViewModel;
    CharacterCreationViewModel characterCreationViewModel;
    AppDatabase mDb;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general, container, false);
        initializeViews(view);
        mDb=AppDatabase.getInstance(requireContext());

        InitializeViewModels();
        setupData();

        return view;
    }

    private void InitializeViewModels() {
        characterCreationViewModel =new ViewModelProvider(requireActivity()).get(CharacterCreationViewModel.class);
        characterClassViewModel=new ViewModelProvider(requireActivity()).get(CharacterClassViewModel.class);
        characterRaceViewModel=new ViewModelProvider(requireActivity()).get(CharacterRaceViewModel.class);
    }

    private void setupData() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final List<ClassEntry> classEntries = mDb.classDao().getClasses();
                final List<String> distinctRaceNames=mDb.RaceDao().getDistinctNames();
                final List<String> raceNames=mDb.RaceDao().getAllRaceNames();

                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        setUpClassPicker(classEntries);
                        setUpRacePicker(distinctRaceNames,raceNames);
                    }
                });
            }
        });
    }




    private void setUpClassPicker(final List<ClassEntry> classEntries) {
        setUpClassPickerData();

        classPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                updateClassText(classEntries.get(newVal));
                updateClassViewModels(classEntries.get(newVal));
            }
        });
    }

    private void setUpClassPickerData() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final List<String> classNames = mDb.classDao().getAllClassNames();

                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        classPicker.setDisplayedValues(classNames.toArray(new String[0]));
                        classPicker.setMinValue(0);
                        classPicker.setMaxValue(classNames.size()-1);
                        characterClassViewModel.getCharacterClass().observe(requireActivity(), new Observer<ClassEntry>() {
                            @Override
                            public void onChanged(final ClassEntry classEntry) {
                                characterClassViewModel.getCharacterClass().removeObserver(this);
                                classPicker.setValue(classNames.indexOf(classEntry.getName()));
                                updateClassText(classEntry);
                            }
                        });
                    }
                });
            }
        });
    }

    private void updateClassViewModels(ClassEntry classEntry) {
        characterClassViewModel.setCharacterClass(classEntry);
        characterCreationViewModel.setCharacterClass(classEntry.getName());
        characterCreationViewModel.setCharacterHitPoints(classEntry.getHitDice());
    }

    private void updateClassText(final ClassEntry classEntry) {
        primaryAbilitiesTextView.setText(classEntry.getPrimaryAbilities());
        String hitDieString = "d" + classEntry.getHitDice();
        hitDieTextView.setText(hitDieString);
    }



    private void setUpRacePicker(final List<String> distinctRaces, final List<String> raceNames) {
        setUpRacePickerData(distinctRaces);

        baseRacePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, final int newVal) {
                if (!raceNames.contains(distinctRaces.get(newVal))) {
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            final List<String> subRaces = mDb.RaceDao().getSubRaces(distinctRaces.get(newVal));

                            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                                @Override
                                public void run() {
                                    subRacePicker.setVisibility(View.VISIBLE);
                                    subRacePicker.setDisplayedValues(subRaces.toArray(new String[0]));
                                    updateChosenRace(subRaces.get(subRacePicker.getValue()));

                                    subRacePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                        @Override
                                        public void onValueChange(NumberPicker picker, int oldVal, final int newVal) {
                                            final String[] displayedValues = picker.getDisplayedValues();
                                            updateChosenRace(displayedValues[newVal]);
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    subRacePicker.setVisibility(View.INVISIBLE);
                    updateChosenRace(distinctRaces.get(newVal));
                }
            }
        });
    }


    private void setUpRacePickerData(final List<String> distinctRaces) {
        baseRacePicker.setDisplayedValues(distinctRaces.toArray(new String[0]));
        baseRacePicker.setMinValue(0);
        baseRacePicker.setMaxValue(8);
        subRacePicker.setMinValue(0);
        subRacePicker.setMaxValue(1);
        subRacePicker.setVisibility(View.INVISIBLE);

        characterRaceViewModel.getCharacterRace().observe(requireActivity(), new Observer<RaceEntry>() {
            @Override
            public void onChanged(final RaceEntry raceEntry) {
                characterRaceViewModel.getCharacterRace().removeObserver(this);

                updateChosenRace(raceEntry.getName());
                baseRacePicker.setValue(distinctRaces.indexOf(raceEntry.getBaseRace()));
                if(Objects.equals(raceEntry.getBaseRace(), raceEntry.getName())){
                    subRacePicker.setVisibility(View.INVISIBLE);
                }
                else{
                    subRacePicker.setVisibility(View.VISIBLE);

                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            final List<String> subRaces=mDb.RaceDao().getSubRaces(raceEntry.getBaseRace());

                            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                                @Override
                                public void run() {
                                    subRacePicker.setDisplayedValues(subRaces.toArray(new String[0]));
                                    subRacePicker.setValue(subRaces.indexOf(raceEntry.getName()));
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    private void updateChosenRace(final String raceName) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final RaceEntry raceEntry = mDb.RaceDao().findRace(raceName);
                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        String SEPARATOR = getSeparator(raceEntry);
                        characterRaceViewModel.setCharacterRace(raceEntry);
                        characterCreationViewModel.setCharacterRace(raceEntry.getName());
                        characterCreationViewModel.setPlayerSpells(raceEntry.getSkills());
                        String bonusLanguage="";
                        if(raceEntry.getBonusLanguage()!=0){
                            bonusLanguage=", +1 Language";
                        }
                        String output=StringListConverter.toString(raceEntry.getAbilityIncrease()) + bonusLanguage + SEPARATOR + StringListConverter.toString(raceEntry.getSkills());
                        raceTraits.setText(output);
                    }
                });
            }
        });
    }

    private String getSeparator(RaceEntry raceEntry) {
        String SEPARATOR=", ";
        if(StringListConverter.toString(raceEntry.getSkills()).isEmpty()){
            SEPARATOR = "";
        }
        return SEPARATOR;
    }

    @Override
    public void onPause() {
        if (characterName.getText() == null || characterName.getText().toString().equals("")) {
            characterName.setText(R.string.unknown_name);
        }
        characterCreationViewModel.setCharacterName(characterName.getText().toString().trim());

        super.onPause();
    }

    private void initializeViews(View view) {
        characterName=view.findViewById(R.id.character_name_editText);
        baseRacePicker=view.findViewById(R.id.base_race_picker);
        subRacePicker=view.findViewById(R.id.sub_race_picker);
        raceTextView=view.findViewById(R.id.race_label);
        classTextView=view.findViewById(R.id.class_pick_textView);
        hitDieTextView=view.findViewById(R.id.hit_die_value);
        primaryAbilitiesTextView=view.findViewById(R.id.primary_abilities_value);
        raceTraits=view.findViewById(R.id.race_traits);
        classPicker=view.findViewById(R.id.class_picker);
    }
}
