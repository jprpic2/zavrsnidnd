package com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dndbeginner.Database.Room.Data.Class.ClassEntry;

public class CharacterClassViewModel extends ViewModel {
    private MutableLiveData<ClassEntry> characterClass=new MutableLiveData<>(new ClassEntry());

    public void setCharacterClass(ClassEntry playerClass){
        this.characterClass.setValue(playerClass);
    }
    public LiveData<ClassEntry> getCharacterClass(){
        return this.characterClass;
    }
}
