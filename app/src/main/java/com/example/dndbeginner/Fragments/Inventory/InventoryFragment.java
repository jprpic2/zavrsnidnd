package com.example.dndbeginner.Fragments.Inventory;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter.PlayerCharacterViewModel;
import com.example.dndbeginner.Fragments.Inventory.ItemsActivities.ArmorActivity;
import com.example.dndbeginner.Fragments.Inventory.ItemsActivities.BackpackActivity;
import com.example.dndbeginner.Fragments.Inventory.ItemsActivities.OffhandActivity;
import com.example.dndbeginner.Fragments.Inventory.ItemsActivities.WeaponsActivity;
import com.example.dndbeginner.R;

    public class InventoryFragment extends Fragment {

        public static final String EXTRA_INT = "EXTRA_INT";
        ImageButton backPackButton;
        ImageButton armorButton;
        ImageButton weaponButton;
        ImageButton shieldButton;

        int playerCharacterID;

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            final View view = inflater.inflate(R.layout.inventory_fragment, container, false);

            final PlayerCharacterViewModel characterViewModel=new ViewModelProvider(requireActivity()).get(PlayerCharacterViewModel.class);
            characterViewModel.getPlayerCharacterEntry().observe(requireActivity(), new Observer<PlayerCharacterEntry>() {
                @Override
                public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                    characterViewModel.getPlayerCharacterEntry().removeObserver(this);
                    playerCharacterID=playerCharacterEntry.getId();
                }
            });
            backPackButton=view.findViewById(R.id.backpack_imagebutton);
            armorButton=view.findViewById(R.id.armor_imagebutton);
            weaponButton=view.findViewById(R.id.weapon_imagebutton);
            shieldButton=view.findViewById(R.id.shield_imagebutton);

            backPackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), BackpackActivity.class);
                    intent.putExtra(EXTRA_INT, playerCharacterID);
                    startActivity(intent);
                }
            });
            armorButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ArmorActivity.class);
                    intent.putExtra(EXTRA_INT, playerCharacterID);
                    startActivity(intent);
                }
            });
            weaponButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), WeaponsActivity.class);
                    intent.putExtra(EXTRA_INT, playerCharacterID);
                    startActivity(intent);
                }
            });
            shieldButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), OffhandActivity.class);
                    intent.putExtra(EXTRA_INT, playerCharacterID);
                    startActivity(intent);
                }
            });
            return view;
        }
    }
