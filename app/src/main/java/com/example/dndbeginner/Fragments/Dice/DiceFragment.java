package com.example.dndbeginner.Fragments.Dice;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.dndbeginner.R;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.util.ArrayList;


public class DiceFragment extends Fragment {

    private NumberPicker diePicker;
    private NumberPicker modPicker;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dice, container, false);
        setUpPickers(view);
        setUpImages(view);

        return view;
    }

    private void setUpImages(final View view) {
        ImageView dieD4 = view.findViewById(R.id.dieD4);
        ImageView dieD6 = view.findViewById(R.id.dieD6);
        ImageView dieD8 = view.findViewById(R.id.dieD8);
        ImageView dieD10 = view.findViewById(R.id.dieD10);
        ImageView dieD12 = view.findViewById(R.id.dieD12);
        ImageView dieD20 = view.findViewById(R.id.dieD20);
        ImageView dieD100 = view.findViewById(R.id.dieD100);

        dieD4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpDieOnClickListener(4,v);
            }
        });
        dieD6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpDieOnClickListener(6,v);
            }
        });
        dieD8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpDieOnClickListener(8,v);
            }
        });
        dieD10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpDieOnClickListener(10,v);
            }
        });
        dieD12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpDieOnClickListener(12,v);
            }
        });
        dieD20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpDieOnClickListener(20,v);
            }
        });
        dieD100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpDieOnClickListener(100,v);
            }
        });
    }

    private void setUpDieOnClickListener(int numberOfSides,View v){
        Die die = new Die(numberOfSides);
        ArrayList<Integer> rolls = getRolls(die);
        PopupRoll popupRoll = new PopupRoll(getDiePickerValue()+"d"+die.getNumberOfSides()+getModifierString(),
                getRollResult(rolls),
                getRollsAsString(rolls));
        popupRoll.showPopupWindow(v);
    }

    private void setUpPickers(View view) {
        diePicker=view.findViewById(R.id.diePicker);
        modPicker=view.findViewById(R.id.modPicker);

        diePicker.setDisplayedValues( new String[] {"1", "2","3","4","5","6","7","8","9","10" } );
        diePicker.setMinValue(0);
        diePicker.setMaxValue(9);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            diePicker.setSelectionDividerHeight(0);
            diePicker.setTextSize(50);
        }
        diePicker.setWrapSelectorWheel(false);
        diePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        modPicker.setMinValue(0);
        modPicker.setMaxValue(10);
        modPicker.setDisplayedValues( new String[] { "-5", "-4", "-3","-2","-1","0","+1","+2","+3","+4","+5" } );
        modPicker.setValue(5);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            modPicker.setSelectionDividerHeight(0);
            modPicker.setTextSize(50);
        }
        modPicker.setWrapSelectorWheel(false);
        modPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

    }

    private int getModPickerValue(){
        return modPicker.getValue()-5;
    }

    private int getDiePickerValue(){
        return diePicker.getValue()+1;
    }

    private ArrayList<Integer> getRolls(Die die){
        ArrayList<Integer> rolls = new ArrayList<>();
        for(int i=0;i<getDiePickerValue();i++){
            rolls.add(die.Roll());
        }
        return rolls;
    }

    private int minimumRoll(int rollValue){
        return Math.max(rollValue, 0);
    }

    private String getModifierString(){
        int modifierValue=getModPickerValue();
        String modifier="";
        if(modifierValue>0){
            modifier="+"+ modifierValue;
        }
        else if(modifierValue<0){
            modifier=String.valueOf(modifierValue);
        }
        return modifier;
    }

    private int getRollResult(ArrayList<Integer> rolls){
        int rollSum=0;
        for (int roll : rolls)
        {
            rollSum += minimumRoll(roll + getModPickerValue());
        }
        return rollSum;
    }

    private String getRollsAsString(ArrayList<Integer> rolls){
        StringBuilder rollsString= new StringBuilder();
        String prefix = "";
        for (int roll : rolls) {
            rollsString.append(prefix);
            prefix = ",";
            rollsString.append(roll);
        }
        return rollsString.toString();
    }
}
