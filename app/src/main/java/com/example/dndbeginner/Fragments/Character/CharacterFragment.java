package com.example.dndbeginner.Fragments.Character;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.Class.ClassEntry;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;
import com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter.PlayerCharacterViewModel;
import com.example.dndbeginner.Fragments.Inventory.ItemsActivities.BackpackActivity;
import com.example.dndbeginner.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CharacterFragment extends Fragment {

    TextView playerStrength;
    TextView playerDexterity;
    TextView playerConstitution;
    TextView playerIntelligence;
    TextView playerWisdom;
    TextView playerCharisma;
    TextView playerName;
    TextView playerClass;
    TextView playerRace;
    TextView playerBackground;
    TextView playerLevel;
    TextView playerExperience;
    TextView nextLevelExperience;
    TextView playerArmorClass;
    TextView playerSpeed;
    TextView playerAttack;
    TextView playerHitPoints;
    TextView playerSkills;
    TextView playerTools;
    TextView playerLanguages;
    TextView playerSpells;
    TextView playerProficiencies;
    Button gainExperienceButton;
    ImageView editableName;
    PlayerCharacterViewModel characterViewModel;

    int hitDiceLevelUP;

    AppDatabase mDb;

    private final List<Integer> experienceNeeded = Arrays.asList(300,900,2700,6500,14000,23000,34000,48000,64000,85000,100000,120000,140000,165000,190000,225000,265000,305000,355000,500000);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_character, container, false);

        mDb=AppDatabase.getInstance(requireContext());
        initializeViews(view);
        

        characterViewModel = new ViewModelProvider(requireActivity()).get(PlayerCharacterViewModel.class);

        characterViewModel.getPlayerCharacterEntry().observe(requireActivity(), new Observer<PlayerCharacterEntry>() {
            @Override
            public void onChanged(final PlayerCharacterEntry playerCharacterEntry) {
                setUpGainEXPButton(playerCharacterEntry);
                setUpEditableName(playerCharacterEntry);

                populateUI(playerCharacterEntry);
                setUpChangeableAbilities();

                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        final ClassEntry playerClass=mDb.classDao().getPlayerClass(playerCharacterEntry.getId());
                        AppExecutors.getInstance().mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                populateClassUI(playerClass);
                            }
                        });
                    }
                });
            }
        });




        return view;
    }

    private void setUpChangeableAbilities() {

    }

    private void setUpEditableName(final PlayerCharacterEntry playerCharacterEntry) {
        View.OnClickListener onClickListener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = LayoutInflater.from(requireActivity()).inflate(R.layout.name_change_alertdialog, null,false);
                final EditText input= (EditText) dialogView.findViewById(R.id.name_change_edittext);

                final AlertDialog.Builder alert = new AlertDialog.Builder(requireContext(), R.style.CustomAlertDialog);
                alert.setView(dialogView);

                alert.setPositiveButton(getResources().getString(R.string.alert_dialog_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!input.getText().toString().isEmpty()){
                            playerCharacterEntry.setName(input.getText().toString().trim());
                            updateCharacter(playerCharacterEntry);
                        }
                        hideKeyboard(dialog);
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        hideKeyboard(dialog);
                    }
                });
                final AlertDialog mDialog = alert.create();
                mDialog.setOnShowListener( new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        mDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(requireActivity().getColor(R.color.colorPrimary));
                        mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(requireActivity().getColor(R.color.colorPrimary));
                    }
                });

                mDialog.show();
                input.requestFocus();
                InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        };
        this.playerName.setOnClickListener(onClickListener);
        this.editableName.setOnClickListener(onClickListener);
    }

    private void updateCharacter(final PlayerCharacterEntry playerCharacterEntry) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.playerCharacterDao().update(playerCharacterEntry);
            }
        });
    }

    private void populateClassUI(ClassEntry playerClass) {
        hitDiceLevelUP=(playerClass.getHitDice()/2)+1;
        ArrayList<String> weaponAndArmorProficiencies=new ArrayList<>(playerClass.getWeaponProficiency());
        if(playerClass.getArmorProficiency()!=null){
            weaponAndArmorProficiencies.addAll(playerClass.getArmorProficiency());
        }
        playerProficiencies.setText(StringListConverter.toString(weaponAndArmorProficiencies));
    }

    private void setUpGainEXPButton(final PlayerCharacterEntry playerCharacterEntry) {
        gainExperienceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = LayoutInflater.from(requireActivity()).inflate(R.layout.gain_exp_alertdialog, null,false);
                final EditText input= (EditText) dialogView.findViewById(R.id.gain_xp_edittext);

                final AlertDialog.Builder alert = new AlertDialog.Builder(requireContext(), R.style.CustomAlertDialog);
                alert.setView(dialogView);


                InputFilter[] filterArray = new InputFilter[1];
                filterArray[0] = new InputFilter.LengthFilter(6);
                input.setFilters(filterArray);
                alert.setPositiveButton(getResources().getString(R.string.alert_dialog_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int currentExperience=playerCharacterEntry.getExperience();
                        int inputExperience=0;
                        if(!input.getText().toString().trim().equals("")){
                            inputExperience=Integer.parseInt(input.getText().toString().trim());
                        }
                        currentExperience=currentExperience+inputExperience;
                        int playerLevel=playerCharacterEntry.getCharacterLevel();
                        if(playerLevel>=20){
                            playerCharacterEntry.setExperience(0);
                            updateCharacter(playerCharacterEntry);
                            hideKeyboard(dialog);
                            dialog.dismiss();
                        }
                        else{
                            if(currentExperience>=experienceNeeded.get(playerLevel-1)){
                                int earnedHitPoints=playerCharacterEntry.getHitPoints();
                                while(currentExperience>=experienceNeeded.get(playerLevel-1)) {
                                    currentExperience=currentExperience-experienceNeeded.get(playerLevel-1);
                                    if(playerLevel<20){
                                        playerLevel++;
                                        earnedHitPoints=earnedHitPoints+(hitDiceLevelUP/2)+1;
                                    }
                                }
                                playerCharacterEntry.setHitPoints(earnedHitPoints);
                                playerCharacterEntry.setExperience(currentExperience);
                                playerCharacterEntry.setCharacterLevel(playerLevel);
                            }
                            else{
                                playerCharacterEntry.setExperience(currentExperience);
                            }
                            updateCharacter(playerCharacterEntry);
                            hideKeyboard(dialog);
                        }
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        hideKeyboard(dialog);
                    }
                });
                final AlertDialog mDialog = alert.create();
                mDialog.setOnShowListener( new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        mDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(requireActivity().getColor(R.color.colorPrimary));
                        mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(requireActivity().getColor(R.color.colorPrimary));
                    }
                });

                mDialog.show();
                input.requestFocus();
                InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        });
    }

    public static String getAbilityModifier(int abilityScore){
        StringBuilder stringBuilder=new StringBuilder();
        if(abilityScore<10){
            stringBuilder.append("-");
            if(abilityScore==1){
                stringBuilder.append("5");
            }
            else if(abilityScore==2 || abilityScore==3){
                stringBuilder.append("4");
            }
            else if(abilityScore==4 || abilityScore==5){
                stringBuilder.append("3");
            }
            else if(abilityScore==6 || abilityScore==7){
                stringBuilder.append("2");
            }
            else if(abilityScore==8 || abilityScore==9){
                stringBuilder.append("1");
            }
        }
        else if(abilityScore==10 || abilityScore==11){
            return stringBuilder.toString();
        }
        else {
            stringBuilder.append("+");
            if(abilityScore==12 || abilityScore==13){
                stringBuilder.append("1");
            }
            else if(abilityScore==14 || abilityScore==15){
                stringBuilder.append("2");
            }
            else if(abilityScore==16 || abilityScore==17){
                stringBuilder.append("3");
            }
            else if(abilityScore==18 || abilityScore==19){
                stringBuilder.append("4");
            }
            else {
                stringBuilder.append("5");
            }
        }
        return stringBuilder.toString();
    }
    @SuppressLint("SetTextI18n")
    private void populateUI(final PlayerCharacterEntry playerCharacterEntry) {
        playerName.setText(playerCharacterEntry.getName());
        playerClass.setText(playerCharacterEntry.getCharacterClass());
        playerRace.setText(playerCharacterEntry.getRace());
        playerBackground.setText(playerCharacterEntry.getBackground());
        playerLevel.setText(String.valueOf(playerCharacterEntry.getCharacterLevel()));
        playerExperience.setText(String.valueOf(playerCharacterEntry.getExperience()));
        nextLevelExperience.setText(String.valueOf(experienceNeeded.get(playerCharacterEntry.getCharacterLevel()-1)));
        playerStrength.setText(playerCharacterEntry.getStrength() + getAbilityModifier(playerCharacterEntry.getStrength()));
        playerDexterity.setText(playerCharacterEntry.getDexterity() + getAbilityModifier(playerCharacterEntry.getDexterity()));
        playerConstitution.setText(playerCharacterEntry.getConstitution() + getAbilityModifier(playerCharacterEntry.getConstitution()));
        playerIntelligence.setText(playerCharacterEntry.getIntelligence() + getAbilityModifier(playerCharacterEntry.getIntelligence()));
        playerWisdom.setText(playerCharacterEntry.getWisdom() + getAbilityModifier(playerCharacterEntry.getWisdom()));
        playerCharisma.setText(playerCharacterEntry.getCharisma() + getAbilityModifier(playerCharacterEntry.getCharisma()));
        playerArmorClass.setText(String.valueOf(playerCharacterEntry.getArmorClass()));
        playerHitPoints.setText(String.valueOf(playerCharacterEntry.getHitPoints()));
        playerAttack.setText(playerCharacterEntry.getAttackDie());
        playerSkills.setText(StringListConverter.toString(playerCharacterEntry.getSkills()));
        playerTools.setText(StringListConverter.toString(playerCharacterEntry.getTools()));
        if(StringListConverter.toString(playerCharacterEntry.getTools()).trim().isEmpty()){
            playerTools.setText(R.string.empty_list_none);
        }
        playerLanguages.setText(StringListConverter.toString(playerCharacterEntry.getLanguages()));
        playerSpells.setText(StringListConverter.toString(playerCharacterEntry.getSpells()));
        if(StringListConverter.toString(playerCharacterEntry.getSpells()).trim().isEmpty()){
            playerSpells.setText(R.string.empty_list_none);
        }


        mDb.RaceDao().getPlayerRace(playerCharacterEntry.getId()).observe(requireActivity(), new Observer<RaceEntry>() {
            @Override
            public void onChanged(final RaceEntry raceEntry) {
                mDb.RaceDao().getPlayerRace(playerCharacterEntry.getId()).removeObserver(this);
                requireActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        playerSpeed.setText(String.valueOf(raceEntry.getSpeed()));
                    }
                });
            }
        });
    }

    private void initializeViews(final View view) {
        editableName=view.findViewById(R.id.edit_name_imageview);
        playerName=view.findViewById(R.id.selected_character_name);
        playerClass=view.findViewById(R.id.selected_character_class);
        playerRace=view.findViewById(R.id.selected_character_race);
        playerBackground=view.findViewById(R.id.selected_character_background);
        playerLevel=view.findViewById(R.id.selected_character_level);
        playerExperience=view.findViewById(R.id.selected_player_experience);
        nextLevelExperience=view.findViewById(R.id.experience_next_level);
        playerStrength=view.findViewById(R.id.selected_player_strength);
        playerDexterity=view.findViewById(R.id.selected_player_dexterity);
        playerConstitution=view.findViewById(R.id.selected_player_constitution);
        playerIntelligence=view.findViewById(R.id.selected_player_intelligence);
        playerWisdom=view.findViewById(R.id.selected_player_wisdom);
        playerCharisma=view.findViewById(R.id.selected_player_charisma);
        playerArmorClass=view.findViewById(R.id.selected_player_armor_class);
        playerSpeed=view.findViewById(R.id.selected_player_speed);
        playerAttack=view.findViewById(R.id.selected_player_attack);
        playerHitPoints=view.findViewById(R.id.selected_player_hit_points);
        playerSkills=view.findViewById(R.id.selected_player_skills);
        playerTools=view.findViewById(R.id.selected_player_tools);
        playerLanguages=view.findViewById(R.id.selected_player_languages);
        playerSpells=view.findViewById(R.id.selected_player_spells);
        playerProficiencies=view.findViewById(R.id.selected_player_proficiencies);
        gainExperienceButton=view.findViewById(R.id.gain_experience_button_fragment);

    }

    private void hideKeyboard(DialogInterface dialog) {
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(InputMethodManager.RESULT_UNCHANGED_HIDDEN, 0);
        dialog.dismiss();
    }

}
