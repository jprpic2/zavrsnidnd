package com.example.dndbeginner.Fragments.Dice;

import java.util.Random;

public class Die {
    private int numberOfSides;
    private Random randomNumberGenerator;

    public Die(int numberOfSides){
        this.numberOfSides=numberOfSides;
        randomNumberGenerator=new Random();
    }

    public int Roll(){
        return randomNumberGenerator.nextInt(numberOfSides)+1;
    }

    public int getNumberOfSides(){
        return this.numberOfSides;
    }
}
