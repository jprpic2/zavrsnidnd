package com.example.dndbeginner.Fragments.Character.AddCharacter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.dndbeginner.Fragments.Character.AddCharacter.Fragments.GeneralInfoFragment;
import com.example.dndbeginner.Fragments.Character.AddCharacter.Fragments.SetupFragment;
import com.example.dndbeginner.Fragments.Character.AddCharacter.Fragments.StatsFragment;
import com.example.dndbeginner.R;

public class AddCharacterFragmentPagerAdapter extends FragmentPagerAdapter {

    private static final int PAGE_COUNT = 3;
    private Context context;

    public AddCharacterFragmentPagerAdapter(@NonNull FragmentManager fm, int behavior, Context context) {
        super(fm, behavior);
        this.context=context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new GeneralInfoFragment();
        } else if (position == 1){
            return new StatsFragment();
        } else {
            return new SetupFragment();
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return context.getString(R.string.fragment_general_info);
        } else if (position == 1){
            return context.getString(R.string.fragment_stats);
        } else {
            return context.getString(R.string.fragment_setup);
        }
    }
}
