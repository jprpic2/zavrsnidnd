package com.example.dndbeginner.Fragments.Inventory.ItemsActivities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.Data.Weapon.WeaponEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;
import com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter.PlayerOwnedItemsViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.WeaponViewModel;
import com.example.dndbeginner.Database.Room.adapters.WeaponAdapter;
import com.example.dndbeginner.Fragments.Inventory.InventoryFragment;
import com.example.dndbeginner.R;

import java.util.ArrayList;
import java.util.List;

public class WeaponsActivity extends AppCompatActivity implements WeaponAdapter.WeaponClickListener {

    private RecyclerView mRecyclerView;
    private AppDatabase mDb;
    private EditText addItemEditText;
    int playerCharacterID;
    private List<OwnedItemEntry> ownedItems;
    private WeaponAdapter mWeaponAdapter;
    private ListView autoCompleteList;
    private List<String> autoCompleteWeapons;
    private TextWatcher textWatcher;
    private FrameLayout autoCompleteFrameLayout;
    View.OnClickListener addItemClickListener;
    private PlayerCharacterEntry playerCharacter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler);

        initializeData();
        setUpAutoComplete();

        final PlayerOwnedItemsViewModel playerOwnedItemsViewModel = new ViewModelProvider(this).get(PlayerOwnedItemsViewModel.class);
        final WeaponViewModel weaponViewModel = new ViewModelProvider(WeaponsActivity.this).get(WeaponViewModel.class);

        loadPlayerCharacter();

        playerOwnedItemsViewModel.getOwnedWeapons(playerCharacterID).observe(WeaponsActivity.this, new Observer<List<OwnedItemEntry>>() {
            @Override
            public void onChanged(final List<OwnedItemEntry> ownedItemEntries) {
                final List<String> ownedWeaponNames = new ArrayList<>();
                ownedItems=new ArrayList<>();

                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        for(final OwnedItemEntry ownedItemEntry : ownedItemEntries){
                            if(!ownedItemEntry.isOffhand()){
                                ownedItems.add(ownedItemEntry);
                                ownedWeaponNames.add(ownedItemEntry.getName());
                            }
                            else{
                                WeaponEntry currentWeapon = mDb.weaponDao().getWeapon(ownedItemEntry.getName());
                                if(isVersatile(currentWeapon)){
                                    ownedItems.add(ownedItemEntry);
                                    ownedWeaponNames.add(ownedItemEntry.getName());
                                }
                            }
                        }
                        AppExecutors.getInstance().mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                weaponViewModel.getOwnedWeapons(ownedWeaponNames).observe(WeaponsActivity.this, new Observer<List<WeaponEntry>>() {
                                    @Override
                                    public void onChanged(List<WeaponEntry> weaponEntries) {
                                        weaponViewModel.getOwnedWeapons(ownedWeaponNames).removeObserver(this);
                                        List<WeaponEntry> shownWeapons = new ArrayList<>();
                                        for (String ownedWeaponName : ownedWeaponNames) {
                                            for (WeaponEntry weaponEntry : weaponEntries) {
                                                if (ownedWeaponName.equals(weaponEntry.getName())) {
                                                    shownWeapons.add(weaponEntry);
                                                }
                                            }
                                        }
                                        mWeaponAdapter.setOwnedWeapons(ownedItems);
                                        mWeaponAdapter.setWeaponEntries(shownWeapons);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        final int position = viewHolder.getAdapterPosition();
                        mDb.ownedItemDao().delete(ownedItems.get(position));

                    }
                });
            }
        }).attachToRecyclerView(mRecyclerView);

        addItemClickListener= new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredString = addItemEditText.getText().toString().trim();
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        OwnedItemEntry newOwnedItem = new OwnedItemEntry(enteredString, "weapon", playerCharacterID, 0,0);
                        mDb.ownedItemDao().insert(newOwnedItem);
                    }
                });
                addItemEditText.setText("");
                addItemEditText.clearFocus();
            }
        };
    }

    private void loadPlayerCharacter() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                playerCharacter=mDb.playerCharacterDao().getStaticPlayerCharacter(playerCharacterID);
            }
        });
    }

    private void setUpAutoComplete() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                autoCompleteWeapons=mDb.weaponDao().getWeaponNames();
                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        final ArrayAdapter<String> arrayAdapter=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,autoCompleteWeapons);
                        autoCompleteList.setAdapter(arrayAdapter);
                        textWatcher=new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                arrayAdapter.getFilter().filter(s);
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if(s.length()==0){
                                    autoCompleteFrameLayout.setVisibility(View.GONE);
                                    addItemEditText.clearFocus();
                                }
                                else {
                                    autoCompleteFrameLayout.setVisibility(View.VISIBLE);
                                }
                            }
                        };
                        addItemEditText.addTextChangedListener(textWatcher);
                        autoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                String armorName=arrayAdapter.getItem(position);
                                addItemEditText.setText(armorName);
                                addItemEditText.setSelection(addItemEditText.getText().length());
                                addItemClickListener.onClick(getCurrentFocus());
                            }
                        });
                    }
                });

            }
        });
    }

    private void initializeData() {
        mDb = AppDatabase.getInstance(getApplicationContext());
        addItemEditText = findViewById(R.id.add_item_edittext);
        addItemEditText.setHint("Add a Weapon");
        autoCompleteList=findViewById(R.id.autoCompleteList);
        mRecyclerView = findViewById(R.id.recycler);
        autoCompleteFrameLayout=findViewById(R.id.autoCompleteFrameLayout);
        mWeaponAdapter=new WeaponAdapter(this,this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mWeaponAdapter);

        mWeaponAdapter.setWeaponEntries(new ArrayList<WeaponEntry>());

        Intent intent = getIntent();
        playerCharacterID = intent.getIntExtra(InventoryFragment.EXTRA_INT, 1);
    }

    @Override
    public void onWeaponClickListener(int position) {
        equipWeapon(ownedItems.get(position),mWeaponAdapter.getWeapons().get(position));
        Log.d("WeaponName",ownedItems.get(position).getName());
    }


    private void equipWeapon(final OwnedItemEntry ownedItemEntry, final WeaponEntry equippedWeapon) {
        if(ownedItemEntry.isEquipped()){
            return;
        }
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final OwnedItemEntry equippedOffhand = mDb.ownedItemDao().getEquippedOffhand(playerCharacterID);
                if(equippedOffhand!=null){
                    updateOffhand(equippedOffhand,equippedWeapon);
                }
            }
        });


        unEquipPreviousWeapon();
        equipNewWeapon(ownedItemEntry);
    }

    private void unEquipPreviousWeapon() {
        for(final OwnedItemEntry ownedItem : ownedItems){
            Log.d("Uneqippable weapon",ownedItem.getName());
            if(ownedItem.isEquipped()){
                Log.d("Unequipping",ownedItem.getName());
                ownedItem.unEquip();
                ownedItem.unEquipOffhand();
                updateOwnedItemDB(ownedItem);
                break;
            }
        }
    }

    private void equipNewWeapon(final OwnedItemEntry ownedItemEntry) {
        ownedItemEntry.setEquipped();
        updateOwnedItemDB(ownedItemEntry);
    }

    private void updateOffhand(OwnedItemEntry equippedOffhand, WeaponEntry equippedWeapon) {
        if(isTwoHanded(equippedWeapon)){
            playerCharacter.setAttackDie(equippedWeapon.getDamage());
            equippedOffhand.unEquipOffhand();
            equippedOffhand.unEquip();
            mDb.ownedItemDao().update(equippedOffhand);
        }
        else{
            if(autoCompleteWeapons.contains(equippedOffhand.getName())){
                WeaponEntry offhandWeapon = mDb.weaponDao().getWeapon(equippedOffhand.getName());
                if(isLight(equippedWeapon)){
                    if(isLight(offhandWeapon)){
                        playerCharacter.setAttackDie(equippedWeapon.getDamage() + " + " + offhandWeapon.getDamage());
                        updatePlayerCharacterDB();
                    }
                    else{
                        equippedOffhand.unEquipOffhand();
                        equippedOffhand.unEquip();
                        mDb.ownedItemDao().update(equippedOffhand);
                    }
                }
                else{
                    equippedOffhand.unEquipOffhand();
                    equippedOffhand.unEquip();
                    mDb.ownedItemDao().update(equippedOffhand);
                }
            }
        }
    }

    private boolean isLight(WeaponEntry equippedWeapon) {
        assert equippedWeapon.getProperties() != null;
        return equippedWeapon.getProperties().toString().toLowerCase().contains("light");
    }

    private boolean isTwoHanded(WeaponEntry equippedWeapon) {
        assert equippedWeapon.getProperties() != null;
        return equippedWeapon.getProperties().toString().toLowerCase().contains("two-handed");
    }

    private boolean isVersatile(WeaponEntry equippedWeapon) {
        assert equippedWeapon.getProperties() != null;
        return equippedWeapon.getProperties().toString().toLowerCase().contains("versatile");
    }

    private void unEquipOffhand(final OwnedItemEntry equippedOffhand) {
        if(!autoCompleteWeapons.contains(equippedOffhand.getName())){
            playerCharacter.setArmorClass(playerCharacter.getArmorClass()-2);
            updatePlayerCharacterDB();
        }

        equippedOffhand.unEquipOffhand();
        updateOwnedItemDB(equippedOffhand);

    }

    private void updatePlayerCharacterDB() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.playerCharacterDao().update(playerCharacter);
            }
        });
    }
    private void updateOwnedItemDB(final OwnedItemEntry ownedItem) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.ownedItemDao().update(ownedItem);
            }
        });
    }
}