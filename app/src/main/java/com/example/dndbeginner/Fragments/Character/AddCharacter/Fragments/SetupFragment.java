package com.example.dndbeginner.Fragments.Character.AddCharacter.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.Background.BackgroundEntry;
import com.example.dndbeginner.Database.Room.Data.Class.ClassEntry;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;
import com.example.dndbeginner.Database.Room.ViewModels.AmmunitionViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.ArmorViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.ShieldViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.ToolViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.WeaponViewModel;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterBackgroundViewModel;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterClassViewModel;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterCreationViewModel;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterRaceViewModel;
import com.example.dndbeginner.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SetupFragment extends Fragment {

    private Button skillsButton;
    private Button toolsButton;
    private Button languagesButton;
    private Button musicButton;
    private TextView skillsTextView;
    private TextView toolsTextView;
    private TextView languagesTextView;
    private TextView numberOfAvailableSkillsTextView;
    private CharacterRaceViewModel characterRaceViewModel;
    private CharacterClassViewModel characterClassViewModel;
    private CharacterBackgroundViewModel characterBackgroundViewModel;
    private CharacterCreationViewModel characterCreationViewModel;
    private ToolViewModel toolViewModel;
    private TextView toolsLabelTextView;

    private Observer<ClassEntry> classEntryObserver;
    private Observer<BackgroundEntry> backgroundEntryObserver;
    private Observer<RaceEntry> raceEntryObserver;

    private boolean[] checkedSkills;
    private String[] listSkills;
    private ArrayList<String> skillsSelected;
    private int numberOfAvailableSkills;
    private int skillCounter;

    private boolean[] checkedTools;
    private boolean[] checkedMusic;
    private List<String> classTools = new ArrayList<>();
    private List<String> backgroundTools = new ArrayList<>();
    private String[] listTools;
    private String[] listMusic;

    private ArrayList<String> toolsSelected;
    private ArrayList<String> musicSelected;
    private int numberOfClassMusic;
    private int numberOfClassTools;
    private int numberOfBackgroundTools;
    private int numberOfBackgroundMusic;
    private int toolCounter;
    private int musicCounter;
    private List<String> knownTools;


    private String[] listLanguages;
    private int numberOfRaceLanguages;
    private int numberOfBackgroundLanguages;
    private int languageCounter;
    private ArrayList<String> languagesSelected;
    private List<String> raceLanguages;
    List<String> languages = Arrays.asList("Common", "Dwarvish", "Elvish", "Giant", "Gnomish", "Goblin", "Halfling", "Orc");
    private boolean[] checkedLanguages;

    AppDatabase mdB;
    List<OwnedItemEntry> startingEquipment;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setup, container, false);
        initializeViews(view);
        characterBackgroundViewModel = new ViewModelProvider(requireActivity()).get(CharacterBackgroundViewModel.class);
        characterClassViewModel = new ViewModelProvider(requireActivity()).get(CharacterClassViewModel.class);
        characterRaceViewModel = new ViewModelProvider(requireActivity()).get(CharacterRaceViewModel.class);
        characterCreationViewModel = new ViewModelProvider(requireActivity()).get(CharacterCreationViewModel.class);
        toolViewModel = new ViewModelProvider(requireActivity()).get(ToolViewModel.class);
        mdB = AppDatabase.getInstance(requireContext());

        if (raceEntryObserver == null) {
            characterRaceViewModel.getCharacterRace().observe(requireActivity(), raceEntryObserver = new Observer<RaceEntry>() {
                @Override
                public void onChanged(RaceEntry raceEntry) {
                    numberOfRaceLanguages = raceEntry.getBonusLanguage();
                    raceLanguages = raceEntry.getLanguages();
                    characterCreationViewModel.getPlayerCharacter().observe(requireActivity(), new Observer<PlayerCharacterEntry>() {
                        @Override
                        public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                            characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                            if (StringListConverter.toString(playerCharacterEntry.getLanguages()).equals("null")) {
                                characterCreationViewModel.setCharacterLanguages(raceLanguages);
                            }
                            languagesTextView.setText(StringListConverter.toString(playerCharacterEntry.getLanguages()));
                        }
                    });
                    setUpLanguageData();
                }
            });
        }

        if (classEntryObserver == null) {
            characterClassViewModel.getCharacterClass().observe(requireActivity(), classEntryObserver = new Observer<ClassEntry>() {
                @Override
                public void onChanged(final ClassEntry classEntry) {
                    numberOfClassTools = classEntry.getNumberOfTools();
                    numberOfClassMusic = classEntry.getNumberOfMusic();
                    numberOfAvailableSkills = classEntry.getNumberOfSkills();
                    if (classEntry.getToolProficiency() != null) {
                        classTools = classEntry.getToolProficiency();
                    }
                    setUpToolsData();
                    setUpMusicData();
                    startingEquipment = changeStartingEquipment(classEntry);


                    characterBackgroundViewModel.getCharacterBackground().observe(requireActivity(), new Observer<BackgroundEntry>() {
                        @Override
                        public void onChanged(BackgroundEntry backgroundEntry) {
                            characterCreationViewModel.setCharacterSkills(backgroundEntry.getSkills());
                            setUpSkillsData(backgroundEntry);
                        }
                    });
                }
            });
        }

        if (backgroundEntryObserver == null) {
            characterBackgroundViewModel.getCharacterBackground().observe(requireActivity(), backgroundEntryObserver = new Observer<BackgroundEntry>() {
                @Override
                public void onChanged(final BackgroundEntry backgroundEntry) {
                    characterCreationViewModel.getPlayerCharacter().observe(requireActivity(), new Observer<PlayerCharacterEntry>() {
                        @Override
                        public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                            characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                            playerCharacterEntry.setBackground(backgroundEntry.getName());
                            skillsTextView.setText(StringListConverter.toString(playerCharacterEntry.getSkills()));
                        }
                    });
                    numberOfBackgroundLanguages = backgroundEntry.getBonusLanguages();
                    numberOfBackgroundMusic = backgroundEntry.getNumberOfMusic();
                    numberOfBackgroundTools = backgroundEntry.getNumberOfTools();
                    if (backgroundEntry.getTools() != null) {
                        backgroundTools = backgroundEntry.getTools();
                    }
                    setUpSkillsData(backgroundEntry);
                    setUpToolsData();
                    setUpMusicData();
                    setUpLanguageData();
                }
            });
        }
        InitializeTextViews();
        setUpSkillsButton();
        setUpLanguagesButton();
        return view;
    }

    private List<OwnedItemEntry> changeStartingEquipment(final ClassEntry classEntry) {

        final List<OwnedItemEntry> startingEquipment = new ArrayList<>();

        final WeaponViewModel weaponViewModel = new ViewModelProvider(requireActivity()).get(WeaponViewModel.class);
        weaponViewModel.getWeaponNames().observe(requireActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> weaponNames) {
                weaponViewModel.getWeaponNames().removeObserver(this);
                for (String weapon : classEntry.getStartingEquipment()) {
                    if (weaponNames.contains(weapon)) {
                        final OwnedItemEntry newOwnedItem = new OwnedItemEntry(weapon, "weapon", 0);
                        startingEquipment.add(newOwnedItem);
                    }
                }
            }
        });
        final ArmorViewModel armorViewModel = new ViewModelProvider(requireActivity()).get(ArmorViewModel.class);
        armorViewModel.getArmorNames().observe(requireActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> armorNames) {
                armorViewModel.getArmorNames().removeObserver(this);
                for (String armor : classEntry.getStartingEquipment()) {
                    if (armorNames.contains(armor)) {
                        final OwnedItemEntry newOwnedItem = new OwnedItemEntry(armor, "armor", 0);
                        startingEquipment.add(newOwnedItem);
                    }
                }
            }
        });

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                List<String> shieldNames = mdB.shieldDao().getShieldNames();
                for(String shield : classEntry.getStartingEquipment()){
                    if(shieldNames.contains(shield)){
                        final OwnedItemEntry newOwnedItem = new OwnedItemEntry(shield, "shield", 0);
                        startingEquipment.add(newOwnedItem);
                    }
                }
            }
        });

        toolViewModel.getAllNames().observe(requireActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> toolNames) {
                toolViewModel.getAllNames().removeObserver(this);
                for (String tool : classEntry.getStartingEquipment()) {
                    if (toolNames.contains(tool)) {
                        OwnedItemEntry newOwnedItem = new OwnedItemEntry(tool, "tool", 0);
                        startingEquipment.add(newOwnedItem);
                    }
                }
            }
        });
        final AmmunitionViewModel ammunitionViewModel = new ViewModelProvider(requireActivity()).get(AmmunitionViewModel.class);
        ammunitionViewModel.getAmmunitionNames().observe(requireActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> ammunitionNames) {
                ammunitionViewModel.getAmmunitionNames().removeObserver(this);
                for (String ammunitionName : classEntry.getStartingEquipment()) {
                    if (ammunitionNames.contains(ammunitionName)) {
                        OwnedItemEntry newOwnedItem = new OwnedItemEntry(ammunitionName, "ammunition", 0);
                        startingEquipment.add(newOwnedItem);
                    }
                }
            }
        });
        return startingEquipment;
    }

    private void setUpLanguageData() {
        characterRaceViewModel.getCharacterRace().observe(requireActivity(), new Observer<RaceEntry>() {
            @Override
            public void onChanged(final RaceEntry raceEntry) {
                characterRaceViewModel.getCharacterRace().removeObserver(this);
                ArrayList<String> languagesForChoosing = new ArrayList<>(languages);
                for (String language : raceLanguages) {
                    languagesForChoosing.remove(language);
                }
                listLanguages = languagesForChoosing.toArray(new String[0]);
                checkedLanguages = new boolean[listLanguages.length];
                languagesSelected = new ArrayList<>(raceEntry.getLanguages());

            }
        });

        updateLanguagesButtonVisibility();
    }

    private void updateLanguagesButtonVisibility() {
        if (numberOfBackgroundLanguages + numberOfRaceLanguages == 0) {
            languagesButton.setVisibility(View.GONE);
        } else {
            languagesButton.setVisibility(View.VISIBLE);
        }
    }

    private void setUpLanguagesButton() {
        updateLanguagesButtonVisibility();
        languagesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languageCounter = 0;
                characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                    @Override
                    public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                        List<String> playerLanguages = playerCharacterEntry.getLanguages();
                        for (int i = 0; i < listLanguages.length; i++) {
                            if (playerLanguages.contains(listLanguages[i])) {
                                checkedLanguages[i] = true;
                                if (!languagesSelected.contains(listLanguages[i])) {
                                    languagesSelected.add(listLanguages[i]);
                                }
                            }
                        }
                    }
                });
                for (boolean checkedItem : checkedLanguages) {
                    if (checkedItem)
                        languageCounter++;
                }
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
                mBuilder.setTitle(R.string.choose_languages);

                mBuilder.setMultiChoiceItems(listLanguages, checkedLanguages, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            if (languageCounter >= numberOfBackgroundLanguages + numberOfRaceLanguages) {
                                checkedLanguages[which] = false;
                                characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                                    @Override
                                    public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                                        for (int i = 0; i < checkedLanguages.length; i++) {
                                            if (checkedLanguages[i]) {
                                                if (!languagesSelected.contains(listLanguages[i])) {
                                                    languagesSelected.add(listLanguages[i]);
                                                }
                                            }
                                        }
                                        playerCharacterEntry.setLanguages(new ArrayList<>(languagesSelected));
                                        languagesTextView.setText(StringListConverter.toString(languagesSelected));
                                    }
                                });
                                dialog.dismiss();
                            } else {
                                languagesSelected.add(listLanguages[which]);
                                languageCounter++;
                            }

                        } else {
                            languageCounter--;
                            languagesSelected.remove(listLanguages[which]);

                        }
                    }
                });
                mBuilder.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        characterCreationViewModel.setCharacterLanguages(new ArrayList<>(languagesSelected));
                        languagesTextView.setText(StringListConverter.toString(languagesSelected));
                    }
                });

                mBuilder.setNegativeButton(R.string.alert_dialog_dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                            @Override
                            public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                                characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                                languagesSelected = new ArrayList<>(playerCharacterEntry.getLanguages());
                                checkedLanguages = new boolean[checkedLanguages.length];
                            }
                        });
                        dialogInterface.dismiss();
                    }
                });

                mBuilder.setNeutralButton(R.string.alert_dialog_clear_all, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        characterRaceViewModel.getCharacterRace().observe(requireActivity(), new Observer<RaceEntry>() {
                            @Override
                            public void onChanged(RaceEntry raceEntry) {
                                characterRaceViewModel.getCharacterRace().removeObserver(this);
                                checkedLanguages = new boolean[checkedLanguages.length];
                                languagesSelected = new ArrayList<>(raceEntry.getLanguages());
                                characterCreationViewModel.setCharacterLanguages(languagesSelected);
                                languagesTextView.setText(StringListConverter.toString(languagesSelected));
                            }
                        });
                    }
                });

                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });

    }


    private void InitializeTextViews() {
        numberOfAvailableSkillsTextView.setText(String.valueOf(numberOfAvailableSkills));
        characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
            @Override
            public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                skillsTextView.setText(StringListConverter.toString(playerCharacterEntry.getSkills()));
                toolsTextView.setText(StringListConverter.toString(playerCharacterEntry.getTools()));
                languagesTextView.setText(StringListConverter.toString(playerCharacterEntry.getLanguages()));
            }
        });
        updateToolsAndMusicVisibility();
    }

    private void setUpMusicData() {
        toolViewModel.getMusicNames().observe(requireActivity(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> strings) {
                toolViewModel.getMusicNames().removeObserver(this);
                musicSelected = new ArrayList<>();
                listMusic = strings.toArray(new String[0]);
                checkedMusic = new boolean[listMusic.length];
            }
        });
    }

    private void setUpToolsData() {
        reSetKnownTools();
        updateToolsAndMusicVisibility();
    }

    private void updateToolsAndMusicVisibility() {
        if (numberOfBackgroundTools + numberOfClassTools == 0) {
            toolsButton.setVisibility(View.GONE);
        } else {
            toolsButton.setVisibility(View.VISIBLE);
            setUpToolsButton();
        }
        if (numberOfBackgroundMusic + numberOfClassMusic == 0) {
            musicButton.setVisibility(View.GONE);
        } else {
            musicButton.setVisibility(View.VISIBLE);
            setUpMusicButton();
        }
        characterCreationViewModel.getPlayerCharacter().observe(requireActivity(), new Observer<PlayerCharacterEntry>() {
            @Override
            public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                if (numberOfBackgroundTools + numberOfClassTools + numberOfBackgroundMusic + numberOfClassMusic == 0 && (playerCharacterEntry.getTools() == null || playerCharacterEntry.getTools().isEmpty())) {
                    toolsLabelTextView.setVisibility(View.GONE);
                    toolsTextView.setVisibility(View.GONE);
                } else {
                    toolsLabelTextView.setVisibility(View.VISIBLE);
                    toolsTextView.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    public void setUpMusicAndTools() {
        ArrayList<String> musicAndTools = new ArrayList<>();
        if (!toolsSelected.isEmpty()) {
            musicAndTools.addAll(toolsSelected);
        }
        if (!musicSelected.isEmpty()) {
            musicAndTools.addAll(musicSelected);
        }
        if (musicAndTools.isEmpty()) {
            musicAndTools = null;
            String chooseProficiencies = getResources().getString(R.string.choose_number) + " " + (numberOfBackgroundMusic + numberOfBackgroundTools + numberOfClassMusic + numberOfClassTools);
            toolsTextView.setText(chooseProficiencies);
        } else {
            toolsTextView.setText(StringListConverter.toString(musicAndTools));
        }
        characterCreationViewModel.setCharacterTools(musicAndTools);

    }

    private void setUpMusicButton() {
        musicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musicCounter = 0;
                characterCreationViewModel.getPlayerCharacter().observe(requireActivity(), new Observer<PlayerCharacterEntry>() {
                    @Override
                    public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                        List<String> playerTools = playerCharacterEntry.getTools();
                        for (int i = 0; i < listMusic.length; i++) {
                            if (playerTools != null && playerTools.contains(listMusic[i])) {
                                checkedMusic[i] = true;
                                if (!musicSelected.contains(listMusic[i])) {
                                    musicSelected.add(listMusic[i]);
                                }
                            }
                        }
                    }
                });
                for (boolean checkedItem : checkedMusic) {
                    if (checkedItem)
                        musicCounter++;
                }
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
                mBuilder.setTitle(R.string.choose_music);

                mBuilder.setMultiChoiceItems(listMusic, checkedMusic, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            if (musicCounter >= numberOfClassMusic + numberOfBackgroundMusic) {
                                checkedMusic[which] = false;
                                characterCreationViewModel.getPlayerCharacter().observe(requireActivity(), new Observer<PlayerCharacterEntry>() {
                                    @Override
                                    public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                                        for (int i = 0; i < checkedMusic.length; i++) {
                                            if (checkedMusic[i]) {
                                                if (!musicSelected.contains(listMusic[i])) {
                                                    musicSelected.add(listMusic[i]);
                                                }
                                            }
                                        }
                                        setUpMusicAndTools();
                                    }
                                });
                                dialog.dismiss();
                            } else {
                                musicSelected.add(listMusic[which]);
                                musicCounter++;
                            }

                        } else {
                            musicCounter--;
                            musicSelected.remove(listMusic[which]);
                        }
                    }
                });
                mBuilder.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        setUpMusicAndTools();
                    }
                });

                mBuilder.setNegativeButton(R.string.alert_dialog_dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        musicSelected = new ArrayList<>();
                        checkedMusic = new boolean[checkedMusic.length];
                        dialogInterface.dismiss();
                    }
                });

                mBuilder.setNeutralButton(R.string.alert_dialog_clear_all, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        checkedMusic = new boolean[checkedMusic.length];
                        musicSelected = new ArrayList<>();
                        setUpMusicAndTools();
                    }
                });
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });
    }

    private void reSetKnownTools() {
        knownTools = new ArrayList<>();
        if (classTools != null) {
            for (String tool : classTools) {
                if (!tool.trim().equals("")) {
                    knownTools.add(tool);
                }
            }
        }
        if (backgroundTools != null) {
            for (String tool : backgroundTools) {
                if (!tool.trim().equals("")) {
                    if (classTools == null || !classTools.contains(tool)) {
                        knownTools.add(tool);
                    }
                }
            }
        }
        if (!knownTools.isEmpty()) {
            toolsTextView.setText(StringListConverter.toString(knownTools));
            characterCreationViewModel.setCharacterTools(knownTools);

            toolViewModel.getToolNames().observe(requireActivity(), new Observer<List<String>>() {
                @Override
                public void onChanged(List<String> strings) {
                    toolViewModel.getToolNames().removeObserver(this);
                    toolsSelected = new ArrayList<>(knownTools);
                    List<String> toolsForChoosing = new ArrayList<>(strings);
                    for (String tool : knownTools) {
                        toolsForChoosing.remove(tool);
                    }
                    listTools = toolsForChoosing.toArray(new String[0]);
                    checkedTools = new boolean[listTools.length];
                }
            });
        } else {
            String chooseProficiencies = getResources().getString(R.string.choose_number) + " " + (numberOfBackgroundMusic + numberOfBackgroundTools + numberOfClassMusic + numberOfClassTools);
            toolsTextView.setText(chooseProficiencies);
            characterCreationViewModel.setCharacterTools(null);
            toolViewModel.getToolNames().observe(requireActivity(), new Observer<List<String>>() {
                @Override
                public void onChanged(List<String> strings) {
                    toolViewModel.getToolNames().removeObserver(this);
                    toolsSelected = new ArrayList<>();
                    listTools = strings.toArray(new String[0]);
                    checkedTools = new boolean[listTools.length];
                }
            });
        }
    }

    private void setUpToolsButton() {
        toolsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolCounter = 0;
                characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                    @Override
                    public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                        List<String> playerTools = playerCharacterEntry.getTools();
                        for (int i = 0; i < listTools.length; i++) {
                            if (playerTools != null && playerTools.contains(listTools[i])) {
                                checkedTools[i] = true;
                                if (!toolsSelected.contains(listTools[i])) {
                                    toolsSelected.add(listTools[i]);
                                }
                            }
                        }
                    }
                });
                for (boolean checkedItem : checkedTools) {
                    if (checkedItem)
                        toolCounter++;
                }

                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
                mBuilder.setTitle(R.string.choose_tools);

                mBuilder.setMultiChoiceItems(listTools, checkedTools, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            if (toolCounter >= numberOfBackgroundTools + numberOfClassTools) {
                                checkedTools[which] = false;
                                characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                                    @Override
                                    public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                                        for (int i = 0; i < checkedTools.length; i++) {
                                            if (checkedTools[i]) {
                                                if (!toolsSelected.contains(listTools[i])) {
                                                    toolsSelected.add(listTools[i]);
                                                }
                                            }
                                        }
                                        setUpMusicAndTools();
                                    }
                                });
                                dialog.dismiss();
                            } else {
                                toolsSelected.add(listTools[which]);
                                toolCounter++;
                            }

                        } else {
                            toolCounter--;
                            toolsSelected.remove(listTools[which]);
                        }
                    }
                });
                mBuilder.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        setUpMusicAndTools();
                    }
                });

                mBuilder.setNegativeButton(R.string.alert_dialog_dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                            @Override
                            public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                                characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                                if (playerCharacterEntry.getTools() != null) {
                                    toolsSelected = new ArrayList<>(playerCharacterEntry.getTools());
                                }
                                checkedTools = new boolean[checkedTools.length];
                            }
                        });
                        dialogInterface.dismiss();
                    }
                });

                mBuilder.setNeutralButton(R.string.alert_dialog_clear_all, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        checkedTools = new boolean[listTools.length];
                        toolsSelected = new ArrayList<>(knownTools);
                        setUpMusicAndTools();
                    }
                });

                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });
    }

    private void setUpSkillsData(final BackgroundEntry backgroundEntry) {
        characterClassViewModel.getCharacterClass().observe(requireActivity(), new Observer<ClassEntry>() {
            @Override
            public void onChanged(ClassEntry classEntry) {
                characterClassViewModel.getCharacterClass().removeObserver(this);
                skillsSelected = new ArrayList<>(backgroundEntry.getSkills());
                List<String> skillListForChoosing = new ArrayList<>(classEntry.getAvailableSkills());
                for (String skill : skillsSelected) {
                    skillListForChoosing.remove(skill);
                }
                listSkills = skillListForChoosing.toArray(new String[0]);
                checkedSkills = new boolean[listSkills.length];
            }
        });
    }

    private void setUpSkillsButton() {
        skillsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skillCounter = 0;
                characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                    @Override
                    public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                        List<String> playerSkills = playerCharacterEntry.getSkills();
                        for (int i = 0; i < listSkills.length; i++) {
                            if (playerSkills.contains(listSkills[i])) {
                                checkedSkills[i] = true;
                                if (!skillsSelected.contains(listSkills[i])) {
                                    skillsSelected.add(listSkills[i]);
                                }
                            }
                        }
                    }
                });
                for (boolean checkedItem : checkedSkills) {
                    if (checkedItem)
                        skillCounter++;
                }
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
                mBuilder.setTitle(R.string.choose_skills);

                mBuilder.setMultiChoiceItems(listSkills, checkedSkills, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            if (skillCounter >= numberOfAvailableSkills) {
                                checkedSkills[which] = false;
                                characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                                    @Override
                                    public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                                        for (int i = 0; i < checkedSkills.length; i++) {
                                            if (checkedSkills[i]) {
                                                if (!skillsSelected.contains(listSkills[i])) {
                                                    skillsSelected.add(listSkills[i]);
                                                }
                                            }
                                        }
                                        playerCharacterEntry.setSkills(new ArrayList<>(skillsSelected));
                                        skillsTextView.setText(StringListConverter.toString(skillsSelected));
                                    }
                                });
                                dialog.dismiss();
                            } else {
                                skillsSelected.add(listSkills[which]);
                                skillCounter++;
                            }

                        } else {
                            skillCounter--;
                            skillsSelected.remove(listSkills[which]);
                        }
                    }
                });
                mBuilder.setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        characterCreationViewModel.setCharacterSkills(new ArrayList<>(skillsSelected));
                        skillsTextView.setText(StringListConverter.toString(skillsSelected));
                    }
                });

                mBuilder.setNegativeButton(R.string.alert_dialog_dismiss, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
                            @Override
                            public void onChanged(PlayerCharacterEntry playerCharacterEntry) {
                                characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                                skillsSelected = new ArrayList<>(playerCharacterEntry.getSkills());
                                checkedSkills = new boolean[checkedSkills.length];
                            }
                        });
                        dialogInterface.dismiss();
                    }
                });

                mBuilder.setNeutralButton(R.string.alert_dialog_clear_all, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        characterBackgroundViewModel.getCharacterBackground().observe(requireActivity(), new Observer<BackgroundEntry>() {
                            @Override
                            public void onChanged(BackgroundEntry backgroundEntry) {
                                characterBackgroundViewModel.getCharacterBackground().removeObserver(this);
                                checkedSkills = new boolean[listSkills.length];
                                skillsSelected = new ArrayList<>(backgroundEntry.getSkills());
                                characterCreationViewModel.setCharacterSkills(skillsSelected);
                                skillsTextView.setText(StringListConverter.toString(skillsSelected));
                            }
                        });
                    }
                });

                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });
    }


    private void initializeViews(View view) {
        skillsButton = view.findViewById(R.id.skills_button);
        toolsButton = view.findViewById(R.id.tools_button);
        languagesButton = view.findViewById(R.id.languages_button);
        skillsTextView = view.findViewById(R.id.skills_proficiencies);
        toolsTextView = view.findViewById(R.id.tool_proficiencies);
        languagesTextView = view.findViewById(R.id.languages);
        numberOfAvailableSkillsTextView = view.findViewById(R.id.number_of_skills_value);
        musicButton = view.findViewById(R.id.music_instruments_button);
        toolsLabelTextView = view.findViewById(R.id.tool_proficiencies_label);
        Button finishButton = view.findViewById(R.id.finish_button);


        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                characterCreationViewModel.getPlayerCharacter().observe(requireActivity(), new Observer<PlayerCharacterEntry>() {
                    @Override
                    public void onChanged(final PlayerCharacterEntry playerCharacterEntry) {
                        characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                        characterRaceViewModel.getCharacterRace().observe(requireActivity(), new Observer<RaceEntry>() {
                            @Override
                            public void onChanged(RaceEntry raceEntry) {
                                characterRaceViewModel.getCharacterRace().removeObserver(this);
                                setAdditionalAbilities(raceEntry, playerCharacterEntry);
                            }
                        });
                        populateLastData(playerCharacterEntry);
                        attachStartingEquipment(playerCharacterEntry);
                    }
                });
                requireActivity().finish();
            }
        });
    }

    private void attachStartingEquipment(final PlayerCharacterEntry playerCharacterEntry) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final long newPlayerID = mdB.playerCharacterDao().insertWithID(playerCharacterEntry);
                for (OwnedItemEntry ownedItemEntry : startingEquipment) {
                    OwnedItemEntry newOwnedItem = new OwnedItemEntry(ownedItemEntry.getName(), ownedItemEntry.getType(), (int) newPlayerID, 0,0);
                    mdB.ownedItemDao().insert(newOwnedItem);
                }
            }
        });
    }

    private void populateLastData(PlayerCharacterEntry playerCharacterEntry) {
        playerCharacterEntry.setGold(0);
        playerCharacterEntry.setSilver(0);
        playerCharacterEntry.setCopper(0);
        playerCharacterEntry.setCharacterLevel(1);
        playerCharacterEntry.setExperience(0);
        playerCharacterEntry.setArmorClass(10);
    }

    private void setAdditionalAbilities(RaceEntry raceEntry, PlayerCharacterEntry playerCharacterEntry) {
        for (String skill : raceEntry.getAbilityIncrease()) {
            if (skill.contains("ALL")) {
                int strength = playerCharacterEntry.getStrength();
                playerCharacterEntry.setStrength(strength + 1);
                int dexterity = playerCharacterEntry.getDexterity();
                playerCharacterEntry.setDexterity(dexterity + 1);
                int constitution = playerCharacterEntry.getConstitution();
                playerCharacterEntry.setConstitution(constitution + 1);
                int intelligence = playerCharacterEntry.getIntelligence();
                playerCharacterEntry.setIntelligence(intelligence + 1);
                int wisdom = playerCharacterEntry.getWisdom();
                playerCharacterEntry.setWisdom(wisdom + 1);
                int charisma = playerCharacterEntry.getCharisma();
                playerCharacterEntry.setCharisma(charisma + 1);
            } else {
                int abilityIncrease = Integer.parseInt(String.valueOf(skill.charAt(1)));
                if (skill.contains("STR")) {
                    int strength = playerCharacterEntry.getStrength();
                    playerCharacterEntry.setStrength(strength + abilityIncrease);
                }
                if (skill.contains("DEX")) {
                    int dexterity = playerCharacterEntry.getDexterity();
                    playerCharacterEntry.setDexterity(dexterity + abilityIncrease);
                }
                if (skill.contains("CON")) {
                    int constitution = playerCharacterEntry.getConstitution();
                    playerCharacterEntry.setConstitution(constitution + abilityIncrease);
                }
                if (skill.contains("INT")) {
                    int intelligence = playerCharacterEntry.getIntelligence();
                    playerCharacterEntry.setIntelligence(intelligence + abilityIncrease);
                }
                if (skill.contains("WIS")) {
                    int wisdom = playerCharacterEntry.getWisdom();
                    playerCharacterEntry.setWisdom(wisdom + abilityIncrease);
                }
                if (skill.contains("CHA")) {
                    int charisma = playerCharacterEntry.getCharisma();
                    playerCharacterEntry.setCharisma(charisma + abilityIncrease);
                }
            }
        }
    }
}