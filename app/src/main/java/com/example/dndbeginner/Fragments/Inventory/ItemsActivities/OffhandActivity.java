package com.example.dndbeginner.Fragments.Inventory.ItemsActivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.CustomItem.CustomItemEntry;
import com.example.dndbeginner.Database.Room.Data.IBackpackItem;
import com.example.dndbeginner.Database.Room.Data.IOffhandItem;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.Data.Shield.ShieldEntry;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolEntry;
import com.example.dndbeginner.Database.Room.Data.Weapon.WeaponEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;
import com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter.PlayerOwnedItemsViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.ShieldViewModel;
import com.example.dndbeginner.Database.Room.adapters.BackpackItemAdapter;
import com.example.dndbeginner.Database.Room.adapters.OffhandItemAdapter;
import com.example.dndbeginner.Fragments.Inventory.InventoryFragment;
import com.example.dndbeginner.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OffhandActivity extends AppCompatActivity implements OffhandItemAdapter.OffhandItemClickListener {

    private RecyclerView mRecyclerView;
    private AppDatabase mDb;
    int playerCharacterID;
    private List<OwnedItemEntry> ownedItems=new ArrayList<>();
    private List<WeaponEntry> ownedWeapons=new ArrayList<>();
    private List<ShieldEntry> ownedShields=new ArrayList<>();
    private OffhandItemAdapter mOffhandAdapter;
    private List<IOffhandItem> offhandItems = new ArrayList<>();
    private WeaponEntry equippedWeapon;
    private PlayerCharacterEntry playerCharacter;

    private OwnedItemEntry equippedOwnedWeapon;

    private EditText addItemEditText;
    private ListView autoCompleteList;
    private List<String> autoCompleteShields;
    private TextWatcher textWatcher;
    private FrameLayout autoCompleteFrameLayout;
    View.OnClickListener addItemClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler);
        InitializeData();
        setUpAutoComplete();

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                playerCharacter=mDb.playerCharacterDao().getStaticPlayerCharacter(playerCharacterID);
                //equippedOwnedWeapon=mDb.ownedItemDao().getEquippedMainHand(playerCharacterID);
            }
        });

        final PlayerOwnedItemsViewModel playerOwnedItemsViewModel = new ViewModelProvider(this).get(PlayerOwnedItemsViewModel.class);

        playerOwnedItemsViewModel.getOwnedWeaponsAndShields(playerCharacterID).observe(this, new Observer<List<OwnedItemEntry>>() {
            @Override
            public void onChanged(final List<OwnedItemEntry> ownedItemEntries) {
                playerOwnedItemsViewModel.getOwnedWeaponsAndShields(playerCharacterID).removeObserver(this);

                Log.d("Tag","Tagging");

                final List<String> shieldNames=new ArrayList<>();
                final List<OwnedItemEntry> shields=new ArrayList<>();
                final List<String> weaponNames=new ArrayList<>();
                final List<OwnedItemEntry> weapons=new ArrayList<>();
                OwnedItemEntry equippedOwnedWeapon = null;

                for(OwnedItemEntry ownedItemEntry : ownedItemEntries){
                    if(ownedItemEntry.getType().equals("shield")){
                        shieldNames.add(ownedItemEntry.getName());
                        shields.add(ownedItemEntry);
                    }
                    else if(ownedItemEntry.getType().equals("weapon")){
                        if(ownedItemEntry.isEquipped()){
                            equippedOwnedWeapon=ownedItemEntry;
                            final OwnedItemEntry finalEquippedOwnedWeapon1 = equippedOwnedWeapon;
                            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                                @Override
                                public void run() {
                                    WeaponEntry currentWeapon=mDb.weaponDao().getWeapon(finalEquippedOwnedWeapon1.getName());
                                    assert currentWeapon.getProperties() != null;
                                    if(currentWeapon.getProperties().toString().toLowerCase().contains("versatile")){
                                        weaponNames.add(currentWeapon.getName());
                                    }
                                    Log.d("Equipped weapon",currentWeapon.getName());
                                }
                            });
                            continue;
                        }
                        weaponNames.add(ownedItemEntry.getName());
                        weapons.add(ownedItemEntry);
                    }
                }

                final OwnedItemEntry finalEquippedOwnedWeapon = equippedOwnedWeapon;
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        List<ShieldEntry> distinctShields=mDb.shieldDao().getShieldsFromNames(shieldNames);

                        ownedShields=new ArrayList<>();
                        for(OwnedItemEntry ownedItemEntry : shields){
                            for(ShieldEntry shieldEntry : distinctShields){
                                if(ownedItemEntry.getName().equals(shieldEntry.getName())){
                                    ownedShields.add(shieldEntry);
                                    break;
                                }
                            }
                        }

                        equippedWeapon=mDb.weaponDao().getWeapon(finalEquippedOwnedWeapon.getName());
                        assert equippedWeapon.getProperties() != null;
                        Log.d("WeaponProperties",equippedWeapon.getProperties().toString());
                        if(equippedWeapon!=null){
                            ownedWeapons=new ArrayList<>();
                            assert equippedWeapon.getProperties() != null;
                            if(equippedWeapon.getProperties().toString().toLowerCase().contains("light")){
                                List<WeaponEntry> distinctWeapons=mDb.weaponDao().getOwnedLightWeapons(weaponNames);
                                for(OwnedItemEntry ownedItemEntry : weapons){
                                    for(WeaponEntry weaponEntry : distinctWeapons){
                                        if(ownedItemEntry.getName().equals(weaponEntry.getName())){
                                            ownedWeapons.add(weaponEntry);
                                            break;
                                        }
                                    }
                                }
                            }
                            else if(equippedWeapon.getProperties().toString().toLowerCase().contains("versatile")){
                                ownedWeapons.add(equippedWeapon);
                                weapons.add(finalEquippedOwnedWeapon);
                            }
                            else if(equippedWeapon.getProperties().toString().toLowerCase().contains("two-handed")){
                                ownedWeapons=new ArrayList<>();
                            }
                        }

                        offhandItems=new ArrayList<>();
                        ownedItems=new ArrayList<>();

                        for(OwnedItemEntry ownedItemEntry : shields){
                            for(ShieldEntry shieldEntry : ownedShields){
                                if(ownedItemEntry.getName().equals(shieldEntry.getName())){
                                    ownedItems.add(ownedItemEntry);
                                    offhandItems.add(shieldEntry);
                                    break;
                                }
                            }
                        }

                        for(OwnedItemEntry ownedItemEntry : weapons){
                            for(WeaponEntry weaponEntry : ownedWeapons){
                                if(ownedItemEntry.getName().equals(weaponEntry.getName())){
                                    ownedItems.add(ownedItemEntry);
                                    offhandItems.add(weaponEntry);
                                    break;
                                }
                            }
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mOffhandAdapter.setOwnedItems(ownedItems);
                                mOffhandAdapter.setOffhandItems(offhandItems);
                            }
                        });

                    }
                });
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        final int position = viewHolder.getAdapterPosition();
                        mDb.ownedItemDao().delete(ownedItems.get(position));
                    }
                });
            }
        }).attachToRecyclerView(mRecyclerView);

        addItemClickListener= new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredString = addItemEditText.getText().toString().trim();
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        if(mDb.weaponDao().getWeaponNames().contains(enteredString)){
                            OwnedItemEntry newOwnedItem = new OwnedItemEntry(enteredString, "weapon", playerCharacterID, 0,0);
                            mDb.ownedItemDao().insert(newOwnedItem);
                        }
                        else if(mDb.shieldDao().getShieldNames().contains(enteredString)){
                            OwnedItemEntry newOwnedItem = new OwnedItemEntry(enteredString, "shield", playerCharacterID, 0,0);
                            mDb.ownedItemDao().insert(newOwnedItem);
                        }
                    }
                });
                addItemEditText.setText("");
                addItemEditText.clearFocus();

            }
        };

    }

    private void setUpAutoComplete() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                autoCompleteShields=mDb.shieldDao().getShieldNames();
                autoCompleteShields.addAll(mDb.weaponDao().getWeaponNames());
                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        final ArrayAdapter<String> arrayAdapter=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_dropdown_item_1line,autoCompleteShields);
                        autoCompleteList.setAdapter(arrayAdapter);
                        textWatcher=new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                arrayAdapter.getFilter().filter(s);
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if(s.length()==0){
                                    autoCompleteFrameLayout.setVisibility(View.GONE);
                                    addItemEditText.clearFocus();
                                }
                                else {
                                    autoCompleteFrameLayout.setVisibility(View.VISIBLE);
                                }
                            }
                        };
                        addItemEditText.addTextChangedListener(textWatcher);
                        autoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                String armorName=arrayAdapter.getItem(position);
                                addItemEditText.setText(armorName);
                                addItemEditText.setSelection(addItemEditText.getText().length());
                                addItemClickListener.onClick(getCurrentFocus());
                            }
                        });
                    }
                });

            }
        });
    }

    private void InitializeData() {
        mDb = AppDatabase.getInstance(getApplicationContext());
        addItemEditText = findViewById(R.id.add_item_edittext);
        addItemEditText.setHint("Add a shield/weapon");
        autoCompleteList=findViewById(R.id.autoCompleteList);
        mRecyclerView = findViewById(R.id.recycler);
        autoCompleteFrameLayout=findViewById(R.id.autoCompleteFrameLayout);
        mOffhandAdapter=new OffhandItemAdapter(this,this);
        mOffhandAdapter.setOffhandItems(new ArrayList<IOffhandItem>());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mOffhandAdapter);

        Intent intent = getIntent();
        playerCharacterID = intent.getIntExtra(InventoryFragment.EXTRA_INT, 1);
    }

    @Override
    public void onOffhandClick(final int position) {
        equipOffhandItem(ownedItems.get(position));
    }

    private void equipOffhandItem(final OwnedItemEntry ownedItemEntry) {
        if(ownedItemEntry.isOffhand()){
            return;
        }
        int previousItemPosition = -10;
        for(final OwnedItemEntry ownedItem : ownedItems){
            if(ownedItem.isOffhand()){
                previousItemPosition=ownedItems.indexOf(ownedItem);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        ownedItem.unEquipOffhand();
                        mDb.ownedItemDao().update(ownedItem);
                    }
                });
            }
        }
        ownedItemEntry.setOffhand();
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.ownedItemDao().update(ownedItemEntry);
            }
        });

        updatePlayerCharacter(ownedItemEntry,previousItemPosition);
    }

    private void updatePlayerCharacter(OwnedItemEntry ownedItemEntry, int previousItemPosition) {
        final IOffhandItem offhandItem = offhandItems.get(ownedItems.indexOf(ownedItemEntry));
        if(previousItemPosition>=0){
            IOffhandItem previousItem=offhandItems.get(previousItemPosition);
            if(previousItem.getClass()==WeaponEntry.class){
                playerCharacter.setAttackDie(equippedWeapon.getDamage());
            }
            else if(previousItem.getClass()==ShieldEntry.class){
                playerCharacter.setArmorClass(playerCharacter.getArmorClass()-2);
            }
        }



        if(offhandItem.getClass()==WeaponEntry.class){
            final String weaponProperties=((WeaponEntry) offhandItem).getProperties().toString().toLowerCase();
            StringBuilder stringBuilder = new StringBuilder(playerCharacter.getAttackDie());
            if(weaponProperties.contains("light")){
                stringBuilder.append(" + ").append(((WeaponEntry) offhandItem).getDamage());
            }
            else if(weaponProperties.contains("versatile")){
                stringBuilder.replace(stringBuilder.indexOf("d")+1,stringBuilder.length(), String.valueOf(Integer.parseInt(stringBuilder.substring(stringBuilder.indexOf("d")+1))+ 2));
            }
            playerCharacter.setAttackDie(stringBuilder.toString());
        }
        else if(offhandItem.getClass()==ShieldEntry.class){
            playerCharacter.setAttackDie(equippedWeapon.getDamage());
            playerCharacter.setArmorClass(playerCharacter.getArmorClass()+2);
        }

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.playerCharacterDao().update(playerCharacter);
            }
        });
    }
}