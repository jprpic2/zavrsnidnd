package com.example.dndbeginner.Fragments.Character.AddCharacter.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.Background.BackgroundEntry;
import com.example.dndbeginner.Database.Room.Data.PlayerCharacter.PlayerCharacterEntry;
import com.example.dndbeginner.Database.Room.Data.Race.RaceEntry;
import com.example.dndbeginner.Database.Room.TypeConverters.StringListConverter;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterBackgroundViewModel;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterCreationViewModel;
import com.example.dndbeginner.Fragments.Character.AddCharacter.ViewModels.CharacterRaceViewModel;
import com.example.dndbeginner.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StatsFragment extends Fragment {

    private NumberPicker strengthPicker;
    private NumberPicker dexterityPicker;
    private NumberPicker constitutionPicker;
    private NumberPicker wisdomPicker;
    private NumberPicker intelligencePicker;
    private NumberPicker charismaPicker;
    private NumberPicker backgroundPicker;
    private TextView backgroundSkills;
    private TextView raceAdditionalAbilities;
    private TextView backgroundBonusLabel;
    private TextView backgroundBonus;
    private LinearLayout raceLayout;
    private CharacterCreationViewModel characterCreationViewModel;
    private Observer<RaceEntry> raceObserver;
    private CharacterBackgroundViewModel characterBackgroundViewModel;
    private AppDatabase mDb;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stats, container, false);
        InitializeViews(view);
        mDb=AppDatabase.getInstance(requireContext());

        characterBackgroundViewModel=new ViewModelProvider(requireActivity()).get(CharacterBackgroundViewModel.class);
        CharacterRaceViewModel characterRaceViewModel = new ViewModelProvider(requireActivity()).get(CharacterRaceViewModel.class);
        characterCreationViewModel = new ViewModelProvider(requireActivity()).get(CharacterCreationViewModel.class);

        InitializePickers();

        if(raceObserver==null){
            characterRaceViewModel.getCharacterRace().observe(requireActivity(),raceObserver=new Observer<RaceEntry>() {
                @Override
                public void onChanged(RaceEntry raceEntry) {
                    if(raceEntry.getName().equals("Half-Elf")){
                        raceLayout.setVisibility(View.VISIBLE);
                        raceAdditionalAbilities.setText(R.string.half_elf_abilities);
                    }
                    else {
                        raceLayout.setVisibility(View.GONE);
                    }
                }
            });
        }

        return view;
    }

    private void InitializeViews(View view) {
        raceAdditionalAbilities=view.findViewById(R.id.race_aditional_ability);
        raceLayout=view.findViewById(R.id.race_additional_layout);
        strengthPicker=view.findViewById(R.id.strength_picker);
        dexterityPicker=view.findViewById(R.id.dexterity_picker);
        constitutionPicker=view.findViewById(R.id.constitution_picker);
        wisdomPicker=view.findViewById(R.id.wisdom_picker);
        intelligencePicker=view.findViewById(R.id.intelligence_picker);
        charismaPicker=view.findViewById(R.id.charisma_picker);
        backgroundPicker=view.findViewById(R.id.background_picker);
        backgroundSkills=view.findViewById(R.id.background_skills);
        backgroundBonusLabel=view.findViewById(R.id.background_bonus_label);
        backgroundBonus=view.findViewById(R.id.background_bonus);
    }

    private void InitializePickers() {
        backgroundPicker.setMinValue(0);
        backgroundPicker.setMaxValue(12);

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final List<BackgroundEntry> backgroundEntries=mDb.backgroundDao().loadAllBackgrounds();
                final List<String> backgroundNames=mDb.backgroundDao().loadAllBackgroundNames();

                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        backgroundPicker.setDisplayedValues(backgroundNames.toArray(new String[0]));

                        characterBackgroundViewModel.getCharacterBackground().observe(requireActivity(), new Observer<BackgroundEntry>() {
                            @Override
                            public void onChanged(BackgroundEntry backgroundEntry) {
                                characterBackgroundViewModel.getCharacterBackground().removeObserver(this);
                                backgroundPicker.setValue(backgroundNames.indexOf(backgroundEntry.getName()));
                                updateBackgroundUI(backgroundEntry);
                            }
                        });

                        backgroundPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                            @Override
                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                characterBackgroundViewModel.setCharacterBackground(backgroundEntries.get(newVal));
                                updateBackgroundUI(backgroundEntries.get(newVal));
                            }
                        });
                    }
                });
            }
        });

        int ID_STRENGTH = 1;
        setUpPicker(strengthPicker, ID_STRENGTH);
        int ID_DEXTERITY = 2;
        setUpPicker(dexterityPicker, ID_DEXTERITY);
        int ID_CONSTITUTION = 3;
        setUpPicker(constitutionPicker, ID_CONSTITUTION);
        int ID_WISDOM = 4;
        setUpPicker(wisdomPicker, ID_WISDOM);
        int ID_INTELLIGENCE = 5;
        setUpPicker(intelligencePicker, ID_INTELLIGENCE);
        int ID_CHARISMA = 6;
        setUpPicker(charismaPicker, ID_CHARISMA);
        
    }

    private void updateBackgroundUI(BackgroundEntry backgroundEntry) {
        backgroundSkills.setText(StringListConverter.toString(backgroundEntry.getSkills()));
        List<String> bonus= new ArrayList<>();
        if(backgroundEntry.getBonusLanguages()==1){
            String bonusLanguages="+1 Language";
            bonus.add(bonusLanguages);
        }
        if(backgroundEntry.getBonusLanguages()==2){
            String bonusLanguages="+2 Languages";
            bonus.add(bonusLanguages);
        }
        if(backgroundEntry.getNumberOfTools()!=0){
            String bonusTools="+1 Tool proficiency";
            bonus.add(bonusTools);
        }
        if(backgroundEntry.getNumberOfMusic()!=0){
            String bonusMusic="+1 Music Instrument";
            bonus.add(bonusMusic);
        }
        if(bonus.isEmpty()){
            backgroundBonusLabel.setVisibility(View.GONE);
            backgroundBonus.setVisibility(View.GONE);
            backgroundBonus.setText(getResources().getText(R.string.empty_list_none));
        }
        else {
            backgroundBonusLabel.setVisibility(View.VISIBLE);
            backgroundBonus.setVisibility(View.VISIBLE);
            backgroundBonus.setText(StringListConverter.toString(bonus));
        }
    }


    private void setUpPicker(final NumberPicker picker, final int picker_ID){
        picker.setDisplayedValues(new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"});
        picker.setMinValue(1);
        picker.setMaxValue(20);
        picker.setWrapSelectorWheel(false);

        characterCreationViewModel.getPlayerCharacter().observe(getViewLifecycleOwner(), new Observer<PlayerCharacterEntry>() {
            @Override
            public void onChanged(final PlayerCharacterEntry playerCharacterEntry) {
                characterCreationViewModel.getPlayerCharacter().removeObserver(this);
                switch(picker_ID){
                    case 1: picker.setValue(playerCharacterEntry.getStrength());
                        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                            @Override
                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                playerCharacterEntry.setStrength(newVal);
                            }
                        });
                        break;
                    case 2:
                        picker.setValue(playerCharacterEntry.getDexterity());
                        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                            @Override
                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                playerCharacterEntry.setDexterity(newVal);
                            }
                        });
                        break;
                    case 3:
                        picker.setValue(playerCharacterEntry.getConstitution());
                        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                            @Override
                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                playerCharacterEntry.setConstitution(newVal);
                            }
                        });
                        break;
                    case 4:
                        picker.setValue(playerCharacterEntry.getWisdom());
                        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                            @Override
                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                playerCharacterEntry.setWisdom(newVal);
                            }
                        });
                        break;
                    case 5:
                        picker.setValue(playerCharacterEntry.getIntelligence());
                        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                            @Override
                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                playerCharacterEntry.setIntelligence(newVal);
                            }
                        });
                        break;
                    case 6:
                        picker.setValue(playerCharacterEntry.getCharisma());
                        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                            @Override
                            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                playerCharacterEntry.setCharisma(newVal);
                            }
                        });
                        break;
                    }

                }});


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            picker.setSelectionDividerHeight(0);
        }
    }
}
