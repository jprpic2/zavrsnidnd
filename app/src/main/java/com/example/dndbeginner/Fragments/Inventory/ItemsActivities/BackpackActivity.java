package com.example.dndbeginner.Fragments.Inventory.ItemsActivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dndbeginner.Database.AppDatabase;
import com.example.dndbeginner.Database.AppExecutors;
import com.example.dndbeginner.Database.Room.Data.CustomItem.CustomItemEntry;
import com.example.dndbeginner.Database.Room.Data.IBackpackItem;
import com.example.dndbeginner.Database.Room.Data.OwnedItem.OwnedItemEntry;
import com.example.dndbeginner.Database.Room.Data.Tools.ToolEntry;
import com.example.dndbeginner.Database.Room.ViewModels.PlayerCharacter.PlayerOwnedItemsViewModel;
import com.example.dndbeginner.Database.Room.ViewModels.ToolViewModel;
import com.example.dndbeginner.Database.Room.adapters.BackpackItemAdapter;
import com.example.dndbeginner.Fragments.Inventory.InventoryFragment;
import com.example.dndbeginner.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class BackpackActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private AppDatabase mDb;
    int playerCharacterID;
    private List<OwnedItemEntry> ownedItems;
    private List<ToolEntry> ownedTools=new ArrayList<>();
    private List<CustomItemEntry> ownedCustomItems=new ArrayList<>();
    private BackpackItemAdapter mBackpackAdatper;
    private FloatingActionButton addItemButton;
    private List<IBackpackItem> backpackItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_button);
        InitializeData();

        final ToolViewModel toolViewModel = new ViewModelProvider(this).get(ToolViewModel.class);
        final PlayerOwnedItemsViewModel playerOwnedItemsViewModel = new ViewModelProvider(this).get(PlayerOwnedItemsViewModel.class);

        playerOwnedItemsViewModel.getOwnedTools(playerCharacterID).observe(this, new Observer<List<OwnedItemEntry>>() {
            @Override
            public void onChanged(List<OwnedItemEntry> ownedItemEntries) {
                final List<String> ownedToolNames = new ArrayList<>();
                ownedItems = ownedItemEntries;
                for (OwnedItemEntry ownedItemEntry : ownedItemEntries) {
                    ownedToolNames.add(ownedItemEntry.getName());
                }
                toolViewModel.getOwnedTools(ownedToolNames).observe(BackpackActivity.this, new Observer<List<ToolEntry>>() {
                    @Override
                    public void onChanged(List<ToolEntry> toolEntries) {
                        toolViewModel.getOwnedTools(ownedToolNames).removeObserver(this);
                        List<ToolEntry> shownTools = new ArrayList<>();
                        for (String ownedToolName : ownedToolNames) {
                            for (ToolEntry toolEntry : toolEntries) {
                                if (ownedToolName.equals(toolEntry.getName())) {
                                    shownTools.add(toolEntry);
                                }
                            }
                        }
                        ownedTools=new ArrayList<>(shownTools);
                        backpackItems=new ArrayList<IBackpackItem>(shownTools);
                        backpackItems.addAll(ownedCustomItems);
                        mBackpackAdatper.setBackpackItems(backpackItems);
                    }
                });
            }
        });

        playerOwnedItemsViewModel.getBackpackCustomItems(playerCharacterID).observe(BackpackActivity.this, new Observer<List<CustomItemEntry>>() {
            @Override
            public void onChanged(List<CustomItemEntry> customItemEntries) {
                backpackItems=new ArrayList<IBackpackItem>(ownedTools);
                ownedCustomItems=customItemEntries;
                backpackItems.addAll(customItemEntries);
                mBackpackAdatper.setBackpackItems(backpackItems);
            }
        });




        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        final int position = viewHolder.getAdapterPosition();
                        final List<IBackpackItem> currentItems = mBackpackAdatper.getBackpackItems();
                        if(currentItems.get(position).getClass()==ToolEntry.class){
                            mDb.ownedItemDao().delete(ownedItems.get(position));
                        }
                        else{
                            mDb.customItemDao().delete((CustomItemEntry) currentItems.get(position));
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                currentItems.remove(position);
                            }
                        });

                    }
                });
            }
        }).attachToRecyclerView(mRecyclerView);

        addItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(BackpackActivity.this,R.style.CustomAlertDialog);
                LayoutInflater inflater = BackpackActivity.this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.add_item_alertdialog, null,false);
                final EditText itemNameEditText= dialogView.findViewById(R.id.item_name_edittext);
                final EditText itemDescriptionEditText= dialogView.findViewById(R.id.item_description_edittext);

                alert.setView(dialogView).
                        setPositiveButton(R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                String enteredName = itemNameEditText.getText().toString().trim();
                                final String enteredString;
                                if(enteredName.equals("")){
                                    enteredString="Unknown Item";
                                }
                                else{
                                    enteredString=enteredName;
                                }
                                toolViewModel.getTools().observe(BackpackActivity.this, new Observer<List<ToolEntry>>() {
                                    @Override
                                    public void onChanged(final List<ToolEntry> toolEntries) {
                                        toolViewModel.getTools().removeObserver(this);
                                        List<String> toolNames = new ArrayList<>();
                                        for (ToolEntry toolEntry : toolEntries) {
                                            toolNames.add(toolEntry.getName());
                                        }
                                        if (toolNames.contains(enteredString)) {
                                            final OwnedItemEntry newOwnedItem = new OwnedItemEntry(enteredString, "tool", playerCharacterID, 0,0);
                                            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mDb.ownedItemDao().insert(newOwnedItem);
                                                }
                                            });
                                            final List<IBackpackItem> shownItems = mBackpackAdatper.getBackpackItems();
                                            for (final ToolEntry toolEntry : toolEntries) {
                                                if (toolEntry.getName().equals(enteredString)) {
                                                    shownItems.add(toolEntry);
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            mBackpackAdatper.setBackpackItems(shownItems);
                                                        }
                                                    });
                                                    break;
                                                }
                                            }
                                        } else {
                                            String enteredDescription = itemDescriptionEditText.getText().toString().trim();
                                            final String description;
                                            if(enteredDescription.equals("")){
                                                description="No description";
                                            }
                                            else{
                                                description=enteredDescription;
                                            }
                                            final CustomItemEntry newCustomItem = new CustomItemEntry(enteredString, description, playerCharacterID);
                                            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mDb.customItemDao().insert(newCustomItem);
                                                }
                                            });
                                            final List<IBackpackItem> shownItems = mBackpackAdatper.getBackpackItems();

                                            shownItems.add(newCustomItem);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    mBackpackAdatper.setBackpackItems(shownItems);
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton(R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });



                final AlertDialog mDialog = alert.create();
                mDialog.setOnShowListener( new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        mDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getColor(R.color.colorPrimary));
                        mDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getColor(R.color.colorPrimary));
                    }
                });
                mDialog.show();
            }
        });

    }

    private void InitializeData() {
        mDb = AppDatabase.getInstance(getApplicationContext());
        addItemButton=findViewById(R.id.add_item_floating_button);
        mRecyclerView = findViewById(R.id.recyclerview_button);
        mBackpackAdatper=new BackpackItemAdapter(new ArrayList<IBackpackItem>());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mBackpackAdatper);

        Intent intent = getIntent();
        playerCharacterID = intent.getIntExtra(InventoryFragment.EXTRA_INT, 1);
    }
}